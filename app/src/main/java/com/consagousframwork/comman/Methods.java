package com.consagousframwork.comman;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Methods {

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    public static void initThreadPolicy() {
        // TODO Auto-generated method stub
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.ThreadPolicy tp = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(tp);
        }
    }

    public static boolean isInternetConnected(Context mContext) {
        try {
            ConnectivityManager connect;
            connect = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connect != null) {
                NetworkInfo resultMobile = connect.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                NetworkInfo resultWifi = connect.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                return (resultMobile != null && resultMobile.isConnectedOrConnecting()) || (resultWifi != null && resultWifi.isConnectedOrConnecting());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isConnected(Context ctx) {

        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches();
        }
    }


    public static void showToasMessage(Context context, String message) {


        Toast.makeText(context, message, Toast.LENGTH_LONG).show();


    }


    public static ProgressDialog progressDailog;


    public static void showProgressDialog(Context context, String message) {

        progressDailog = new ProgressDialog(context);
        progressDailog.setMessage(message);
        progressDailog.setCancelable(false);
        progressDailog.show();

    }

    public static void hideProgressDialog() {

        if (progressDailog != null && progressDailog.isShowing()) {

            progressDailog.dismiss();

        }

    }

    public static void printLog(String message) {

        Log.e("Exception", "Exception Message: " + message);


    }


    public static String getBase64String(Bitmap bitmap) {

        String encoded = "";
        try {


            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        } catch (Exception e) {


        }


        return "";
    }



    public static LatLng getCurrentLatLogn(Context context){



        return null;
    }



    public static String getDirectionsUrl(LatLng origin,LatLng dest, ArrayList<LatLng> markerPoints){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";


       /* if(markerPoints.size()>8){

            for(int i=(markerPoints.size()-8);i<(markerPoints.size()-1);i++){
                LatLng point  = (LatLng) markerPoints.get(i);
                if(i==1)
                    waypoints = "waypoints=";
                waypoints += point.latitude + "," + point.longitude + "|";
            }



        }else {

            for(int i=1;i<(markerPoints.size()-1);i++){
                LatLng point  = (LatLng) markerPoints.get(i);
                if(i==1)
                    waypoints = "waypoints=";
                waypoints += point.latitude + "," + point.longitude + "|";
            }


        }
*/
        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor+"&"+waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters+"&mode=driving";



        Log.e("URL", "URL: "+url);
        return url;
    }




    public static String getTempraturInKelvinToFahrenheit(double temp){

        String str_temp="";

        try {

            double d = 1.8 *(temp-273)+32;

            DecimalFormat df = new DecimalFormat("#.##");
            str_temp = ""+(df.format(temp));

        }catch (Exception e){

            printLog(e.getMessage());
        }


        return str_temp;


    }

    public static String getTempraturInKelvinToCelsius(double temp){

        String str_temp="";

        try {

            double d = (temp-273.15);

            DecimalFormat df = new DecimalFormat("#.##");
            str_temp = ""+(df.format(temp));

        }catch (Exception e){

            printLog(e.getMessage());
        }


        return str_temp;


    }




}
