package com.consagousframwork.server_connection;

import android.util.Log;

import com.consagousframwork.comman.Urls;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApiCalls {
    OkHttpClient client = getClient();


    private OkHttpClient getClient() {
        client = new OkHttpClient.Builder()
                .connectTimeout(7000, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.MINUTES)

                .build();
        return client;
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public String postMethod(String jsonObject, String url, String uidNumber, String uidType ) throws IOException {


        HashMap<String, String> hash = new HashMap<>();



        RequestBody body = RequestBody.create(JSON, jsonObject);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader(Urls.UID_NAUMBER, uidNumber)
                .addHeader(Urls.UID_TYPE, uidType)

                .build();
        Response response = client.newCall(request).execute();

       // Log.e("", "Responce: "+response.body().string());
        return response.body().string();
    }

    public String postMethod(String jsonObject, String url, String uidNumber, String uidType, String uidToken ) throws IOException {



        RequestBody body = RequestBody.create(JSON, jsonObject);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader(Urls.UID_NAUMBER, uidNumber)
                .addHeader(Urls.UID_TYPE, uidType)
                .addHeader(Urls.UID_TOKEN, uidToken)

                .build();
        Response response = client.newCall(request).execute();

        // Log.e("", "Responce: "+response.body().string());
        return response.body().string();
    }



    public String getMethod( String url, String uidNumber, String uidType ) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader(Urls.UID_NAUMBER, uidNumber)
                .addHeader(Urls.UID_TYPE, uidType)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    public String getWeaterMethod( String url) throws IOException {


        Log.e("fgfdg", "Weather APi: "+url );
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    public String postMethod(String userId, String url, String uidNumber, String uidType, File image) throws IOException {


        final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("user_img", "image.png", RequestBody.create(MEDIA_TYPE_PNG, image))
                .addFormDataPart("userId", userId)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .addHeader(Urls.UID_NAUMBER, uidNumber)
                .addHeader(Urls.UID_TYPE, uidType)
                .build();



        Response response = client.newCall(request).execute();

        // Log.e("", "Responce: "+response.body().string());
        return response.body().string();
    }



}

