package com.consagousframwork.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;

@SuppressLint("NewApi")
public class MyTextView extends  android.support.v7.widget.AppCompatTextView{

	 public MyTextView(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	        init();
	    }

	    public MyTextView(Context context, AttributeSet attrs) {
	        super(context, attrs);
	        init();
	    }

	    public MyTextView(Context context,boolean flag) {
	        super(context);
	        	init();
	    }

	    private void init() {
	       /* Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"AvenirNextLTPro-Regular.otf");
	        setTypeface(tf);*/
	    }
	    
	    public void setBold(){
//	    	Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"AvenirNextLTPro-Bold.otf");
//	        setTypeface(tf);
	    }
}
