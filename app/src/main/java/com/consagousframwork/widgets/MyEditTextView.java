package com.consagousframwork.widgets;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by ADMIN1 on 3/7/2017.
 */

public class MyEditTextView extends  android.support.v7.widget.AppCompatEditText{

    public MyEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditTextView(Context context,boolean flag) {
        super(context);
        init();
    }

    private void init() {
	       /* Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"AvenirNextLTPro-Regular.otf");
	        setTypeface(tf);*/
    }

    public void setBold(){
//	    	Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"AvenirNextLTPro-Bold.otf");
//	        setTypeface(tf);
    }
}
