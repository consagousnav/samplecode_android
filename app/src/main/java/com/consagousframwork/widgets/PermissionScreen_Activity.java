package com.consagousframwork.widgets;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.consagousframwork.splesh.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;

public class PermissionScreen_Activity extends Activity {


    private Intent intent;
    private String str_value = "";
    private boolean imageFlage = true;
    private int requestCode, resultCode;
    private Intent data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_permissionscreen);
        intent = getIntent();
        str_value = intent.getStringExtra("Value");


        if (str_value.trim().equalsIgnoreCase("Gallery")) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {


                    imageFlage = false;
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1000);
                } else {

                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 100);
                }
            } else {

                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }


        } else if (str_value.trim().equalsIgnoreCase("Camera")) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {


                    imageFlage = false;
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1001);
                } else {

                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, 200);
                }
            } else {

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 200);
            }

        } else if (str_value.trim().equalsIgnoreCase("Phone")) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1002);
                } else {
                    // continue with your code
                    callPhone(intent.getStringExtra("No"));

                }
            } else {
                // continue with your code
                callPhone(intent.getStringExtra("No"));

            }


        } else if (str_value.trim().equalsIgnoreCase("Mail")) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 1003);
                } else {

                    sendMail(intent.getStringExtra("MainID"));
                }
            } else {

                sendMail(intent.getStringExtra("MainID"));
            }


        } else if (str_value.trim().equalsIgnoreCase("Map")) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, 1004);
                } else {

                    getDirectiononMap();
                }
            } else {

                getDirectiononMap();
            }


        } else if (str_value.trim().equalsIgnoreCase("latlong")) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, 1005);
                } else {

                    getCurrentLatlong();
                }
            } else {

                getCurrentLatlong();
            }


        }


    }

    //************* Activity for result *********************************************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        this.data = data;
        this.requestCode = requestCode;
        this.resultCode = resultCode;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                imageFlage = true;

                if (str_value.trim().equalsIgnoreCase("Gallery")) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1000);


                } else if (str_value.trim().equalsIgnoreCase("Camera")) {

                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1001);

                }

            } else {
                // continue with your code

                if (resultCode == RESULT_OK) {
                    if (str_value.trim().equalsIgnoreCase("Gallery")) {
                        Uri selectedImage = this.data.getData();
                        Intent intent = new Intent();
                        intent.putExtra("ImageURI", selectedImage.toString());
                        setResult(RESULT_OK, intent);
                        finish();
                    } else if (str_value.trim().equalsIgnoreCase("Camera")) {


                        Bitmap bmp = (Bitmap) this.data.getExtras().get("data");
                        Intent intent = new Intent();
                        intent.putExtra("BitmapImage", bmp);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } else {
                    finish();
                }

            }
        } else {
            // continue with your code
            if (resultCode == RESULT_OK) {
                if (str_value.trim().equalsIgnoreCase("Gallery")) {
                    Uri selectedImage = this.data.getData();
                    Intent intent = new Intent();
                    intent.putExtra("ImageURI", selectedImage.toString());
                    setResult(RESULT_OK, intent);
                    finish();
                } else if (str_value.trim().equalsIgnoreCase("Camera")) {


                    Bitmap bmp = (Bitmap) this.data.getExtras().get("data");
                    Intent intent = new Intent();
                    intent.putExtra("BitmapImage", bmp);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } else {
                finish();
            }

        }

    }


    //***************Run time permission ***************************************************************************

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        Intent i = null;

        switch (requestCode) {
            case 1000:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");

                    if (imageFlage) {

                        if (resultCode == RESULT_OK) {
                            Uri selectedImage = this.data.getData();
                            Intent intent = new Intent();
                            intent.putExtra("ImageURI", selectedImage.toString());
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {

                            finish();
                        }


                    } else {
                        i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);


                    }


                } else {
                    Log.e("Permission", "Denied");

                    Toast.makeText(this, "Gallery permission denied", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;


            case 1001:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");

                    if (imageFlage) {

                        if (resultCode == RESULT_OK) {
                            Bitmap bmp = (Bitmap) this.data.getExtras().get("data");
                            Intent intent = new Intent();
                            intent.putExtra("BitmapImage", bmp);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {

                            finish();
                        }


                    } else {
                        i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);


                    }


                } else {
                    Log.e("Permission", "Denied");

                    Toast.makeText(this, "Camera permission denied", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;


            case 1002:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");

                    callPhone(intent.getStringExtra("No"));

                } else {
                    Toast.makeText(this, "Phone call permission denied", Toast.LENGTH_SHORT).show();
                    Log.e("Permission", "Denied");
                    finish();

                }

                break;


            case 1003:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");

                    sendMail(intent.getStringExtra("MainID"));

                } else {
                    Toast.makeText(this, "Send mail permission denied", Toast.LENGTH_SHORT).show();
                    Log.e("Permission", "Denied");
                    finish();

                }

                break;


            case 1004:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");

                    //getCurrentLatLong();

                } else {
                    Toast.makeText(this, " GPRS permission denied", Toast.LENGTH_SHORT).show();
                    Log.e("Permission", "Denied");
                    finish();

                }


                break;


            case 1005:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");

                    LatLng latlong = getCurrentLatlong();

                    Intent intent = new Intent();
                    intent.putExtra("lat", latlong);
                    setResult(RESULT_OK, intent);
                    finish();

                } else {
                    Toast.makeText(this, " GPRS permission denied", Toast.LENGTH_SHORT).show();
                    Log.e("Permission", "Denied");
                    finish();

                }
break;

        }


    }


    //............................ For Rotate Image ............................................

    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.i("RotateImage", "Exif orientation: " + orientation);
            Log.i("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    //************ Call phone *************************************************************************************
    private void callPhone(String str_phoneno) {


        Log.e("Value", "NO: " + str_phoneno);

		/*Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:"+str_phoneno));
		startActivity(intent);*/
        finish();

    }

    //******************** send mail   ************************************************************************
    private void sendMail(String str_mail) {


        Intent intent = null;

        String[] TO = {str_mail};
        intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mailto:"));
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, TO);
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
            finish();


        } catch (android.content.ActivityNotFoundException ex) {

/*

			SnackbarManager.show(
					Snackbar.with(PermissionScreen_Activity.this)
					.text("Unable to mail")
					.type(SnackbarType.MULTI_LINE)
					.animation(false)
					.textColor(getResources().getColor(R.color.error_message_text_color))
					.color(getResources().getColor(R.color.error_message_background_color))
					.position(SnackbarPosition.TOP)
					.duration(5000));

*/
            finish();

        }
    }


    //******************** send mail   ************************************************************************
    private void callMap(String str_slat, String str_slng, String str_elat, String str_elng) {


        try {
            intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=" + str_slat + "," + str_slng + "&daddr=" + str_elat + "," + str_elng));
            startActivity(intent);
            finish();


        } catch (android.content.ActivityNotFoundException ex) {


			/*SnackbarManager.show(
                    Snackbar.with(PermissionScreen_Activity.this)
					.text("Unable to goto map")
					.type(SnackbarType.MULTI_LINE)
					.animation(false)
					.textColor(getResources().getColor(R.color.error_message_text_color))
					.color(getResources().getColor(R.color.error_message_background_color))
					.position(SnackbarPosition.TOP)
					.duration(5000));*/
            finish();


        }
    }


    //**** getCurrentLatLong *************************************************

    private LatLng getDirectiononMap() {

        LatLng latLong = null;
        /*GPSTracker gpsTracker = null;
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.consagousframwork.widgets.PermissionScreen_Activity");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }

        gpsTracker = new GPSTracker(PermissionScreen_Activity.this);

        if (gpsTracker.canGetLocation()) {

            latLong = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            Latitude = (gpsTracker.latitude);
            Longitude = (gpsTracker.longitude);
            Log.v("", "Impossible to connect to LocationManager:Lat: " + Latitude);
            Log.v("", "Impossible to connect to LocationManager:Long: " + Longitude);


            Log.v("", "Impossible to connect to LocationManager:Lat: " + latLong.latitude);
            Log.v("", "Impossible to connect to LocationManager:Long: " + latLong.longitude);

//			callMap(""+latLong.latitude, ""+latLong.longitude, intent.getStringExtra("Elat"), intent.getStringExtra("Elng"));


        } else {

            Log.e("", "Impossible to connect to LocationManager:Long: ");

            //showSettingsAlert();
        }
*/

        return latLong;

    }



    private LatLng getCurrentLatlong(){

        LatLng latLong=null;

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        String  mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                Location location = locationManager.getLastKnownLocation(mprovider);
                latLong = new LatLng(location.getLatitude(), location.getLongitude());
                if (location == null)
                    Toast.makeText(getBaseContext(), "No Location Provider Found Check Your Code", Toast.LENGTH_SHORT).show();

            }
           // locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

           }
        finish();
        return latLong;
    }


}
