package com.consagousframwork.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.consagousframwork.interfaces.ItemTouchHelperAdapter;
import com.consagousframwork.model.UserContact_Been;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;
import com.consagousframwork.widgets.TextAwesome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ADMIN1 on 3/21/2017.
 */

public class UserContact_Adapter extends RecyclerView.Adapter<UserContact_Adapter.ViewHolder> implements ItemTouchHelperAdapter {

    List<UserContact_Been> contactList = new ArrayList<UserContact_Been>();
    List<UserContact_Been> itemList = new ArrayList<UserContact_Been>();

    Context context;
    private String str_flage = "";


    public UserContact_Adapter(Context context, List<UserContact_Been> contactList) {

        this.contactList = contactList;
        this.itemList.addAll(contactList);
        this.context = context;


    }


    @Override
    public UserContact_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);


        return vh;
    }

    @Override
    public void onBindViewHolder(UserContact_Adapter.ViewHolder holder, int position) {


        holder.tv_name.setText(itemList.get(position).getName());
        holder.tv_number.setText(itemList.get(position).getMobileNo());


        if (!str_flage.trim().equalsIgnoreCase(itemList.get(position).getContactType())) {

            str_flage = itemList.get(position).getContactType();
            holder.tv_title.setText(itemList.get(position).getContactType());
//            holder.tv_title.setVisibility(View.VISIBLE);
        } else {

            holder.tv_title.setVisibility(View.GONE);

        }


    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.Contact_Row_Name)
        MyTextView tv_name;

        @Bind(R.id.Contact_Row_Number)
        MyTextView tv_number;

        @Bind(R.id.Contact_Row_Check)
        TextAwesome tv_check;

        @Bind(R.id.Contact_Row_Title)
        MyTextView tv_title;


        @Bind(R.id.Contact_Row)
        LinearLayout ll_row;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

    }

/*
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

       *//* itemList.clear();
        if (charText.length() == 0) {


            itemList.addAll(contactList);

        } else {

            for (int i = 0; i < contactList.size(); i++) {
                if (contactList.get(i).getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    itemList.add(new Contact_Been(contactList.get(i).getName(), contactList.get(i).getNumber(), contactList.get(i).getIcCheck(), contactList.get(i).getPosition()));
                }
            }
        }*//*
        notifyDataSetChanged();
    }*/


    //*********************** Item Touch Helper Adapter interface method *************************************************************************************************************************************
    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {

        Collections.swap(itemList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
//        return false;
    }

    @Override
    public void onItemDismiss(int position) {


        Log.e("sddfds", "Hello:   "+position);
        itemList.remove(position);
        contactList.remove(position);

        notifyItemRemoved(position);

//        notifyDataSetChanged();
    }



}
