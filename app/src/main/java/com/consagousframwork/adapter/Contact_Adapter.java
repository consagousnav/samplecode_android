package com.consagousframwork.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.consagousframwork.model.Contact_Been;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;
import com.consagousframwork.widgets.TextAwesome;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ADMIN1 on 3/21/2017.
 */

public class Contact_Adapter extends RecyclerView.Adapter<Contact_Adapter.ViewHolder> {

    List<Contact_Been> contactList = new ArrayList<Contact_Been>();
    List<Contact_Been> itemList = new ArrayList<Contact_Been>();

    Context context;


    public Contact_Adapter(Context context, List<Contact_Been> contactList) {

        this.contactList = contactList;
        this.itemList.addAll(contactList);
        this.context = context;


    }


    @Override
    public Contact_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(context, v);


        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        if (itemList.get(position).getIcCheck() == 1) {

            holder.tv_check.setAlpha(1);


        } else {

            holder.tv_check.setAlpha(0);

        }

        holder.tv_name.setText(itemList.get(position).getName());
        holder.tv_number.setText(itemList.get(position).getNumber());


        holder.ll_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (itemList.get(position).getIcCheck() == 1) {

                    itemList.set(position, new Contact_Been(itemList.get(position).getName(), itemList.get(position).getNumber(), 0, itemList.get(position).getPosition()));

                    contactList.set(itemList.get(position).getPosition(), new Contact_Been(contactList.get(itemList.get(position).getPosition()).getName(), contactList.get(itemList.get(position).getPosition()).getNumber(), 0, contactList.get(itemList.get(position).getPosition()).getPosition()));

                    holder.tv_check.setAlpha(0);


                } else {

                    itemList.set(position, new Contact_Been(itemList.get(position).getName(), itemList.get(position).getNumber(), 1, itemList.get(position).getPosition()));
                    contactList.set(itemList.get(position).getPosition(), new Contact_Been(contactList.get(itemList.get(position).getPosition()).getName(), contactList.get(itemList.get(position).getPosition()).getNumber(), 1, contactList.get(itemList.get(position).getPosition()).getPosition()));

                    holder.tv_check.setAlpha(1);

                }

            }
        });


    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.Contact_Row_Name)
        MyTextView tv_name;

        @Bind(R.id.Contact_Row_Number)
        MyTextView tv_number;

        @Bind(R.id.Contact_Row_Check)
        TextAwesome tv_check;

        @Bind(R.id.Contact_Row)
        LinearLayout ll_row;


        public ViewHolder(Context context, View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

        itemList.clear();
        if (charText.length() == 0) {


            itemList.addAll(contactList);

        } else {

            for (int i = 0; i < contactList.size(); i++) {
                if (contactList.get(i).getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    itemList.add(new Contact_Been(contactList.get(i).getName(), contactList.get(i).getNumber(), contactList.get(i).getIcCheck(), contactList.get(i).getPosition()));
                }
            }
        }
        notifyDataSetChanged();
    }


}
