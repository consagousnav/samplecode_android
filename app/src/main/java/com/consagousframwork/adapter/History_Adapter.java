package com.consagousframwork.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.consagousframwork.interfaces.ItemClickListener;
import com.consagousframwork.model.History_Been;
import com.consagousframwork.others.DateAndTimeFormateConvertion;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ADMIN1 on 3/16/2017.
 */

public class History_Adapter extends RecyclerView.Adapter<History_Adapter.ViewHolder> {

    List<History_Been> histroryList = new ArrayList<History_Been>();
    Context context;
    ItemClickListener itemClick;




    public History_Adapter(Context context, List<History_Been> histroryList , ItemClickListener itemClick ){

        this.histroryList = histroryList;
        this.context = context;
        this.itemClick = itemClick;


    }


    @Override
    public History_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(context ,v);


        return vh;
    }

    @Override
    public void onBindViewHolder(History_Adapter.ViewHolder holder, final int position) {

        holder.tv_activityId.setText(""+histroryList.get(position).getActivityId());


        holder.tv_endTime.setText(""+DateAndTimeFormateConvertion.getScheduleAdddateFromDate(histroryList.get(position).getEndTime()));

        holder.tv_source.setText(""+histroryList.get(position).getSource());
        holder.tv_destination.setText(""+histroryList.get(position).getDestination());

        holder.tv_startTime.setText(""+DateAndTimeFormateConvertion.getScheduleAdddateFromDate(histroryList.get(position).getStartTime()));


        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                itemClick.onClick(v, position, false);
            }
        });



    }

    @Override
    public int getItemCount() {
        return histroryList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.History_Row_ActivityId)
        MyTextView tv_activityId;

        @Bind(R.id.History_Row_StartTime)
        MyTextView tv_startTime;

        @Bind(R.id.History_Row_EndTime)
        MyTextView tv_endTime;

        @Bind(R.id.History_Row_View)
        MyTextView tv_view;

        @Bind(R.id.History_Row_Source)
        MyTextView tv_source;
        @Bind(R.id.History_Row_Destination)
        MyTextView tv_destination;




        public ViewHolder(Context context, View v) {
            super(v);
            ButterKnife.bind(this,v);

        }

    }

}
