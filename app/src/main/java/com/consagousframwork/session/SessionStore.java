package com.consagousframwork.session;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by ACS on 21-10-15.
 */
public class SessionStore {


	// Message Count
	public static String COUNT="Count";

	// token
	public static String TOKENKEY="tokenkey";
	public static String SAVETOKEN="savetoken";

	// popup
	public static String STATUS="status";
	
	
	


	public static final String USER_ID = "userId";
	public static final String USER_FIRSTNAME = "userFirstName";
	public static final String USER_LASTNAME = "userLastName";
	public static final String USER_EMAILID= "userEmialId";
	public static final String ACCESSTOKEN= "accessToken";
	public static final String USER_MOBILENO= "userMobileNo";

	public static final String USER_DOB= "userDob";
	public static final String USER_SEX= "userSex";
	public static final String USER_AGE= "userAge";
	public static final String USER_COUNTRY= "userCountry";
	public static final String USER_CITY= "userCity";
	public static final String USER_ADDRESS= "userAddress";
	public static final String USER_LAT= "userLat";
	public static final String USER_LONG= "userLong";
	public static final String USER_PROFILEIMAGE= "userProfileImage";







	public static boolean save(Context context, String name, String userId , String userFirstName, String userLastName ,String userEmialId, String accessToken, String userMobileNo
			, String userDob, String userSex, String userAge, String userCountry, String userCity, String userAddress, String userLat
			, String userLong, String userProfileImage

	) {
		SharedPreferences.Editor editor =context.getSharedPreferences(name, Context.MODE_PRIVATE).edit();
		editor.putString(USER_ID, userId);
		editor.putString(USER_FIRSTNAME, userFirstName);
		editor.putString(USER_LASTNAME, userLastName);
		editor.putString(USER_EMAILID, userEmialId);
		editor.putString(ACCESSTOKEN, accessToken);
		editor.putString(USER_MOBILENO, userMobileNo);
		editor.putString(USER_COUNTRY, userCountry);

		editor.putString(USER_DOB, userDob);
		editor.putString(USER_SEX, userSex);
		editor.putString(USER_AGE, userAge);
		editor.putString(USER_CITY, userCity);
		editor.putString(USER_ADDRESS, userAddress);
		editor.putString(USER_LAT, userLat);
		editor.putString(USER_LONG, userLong);
		editor.putString(USER_PROFILEIMAGE, userProfileImage);





		return editor.commit();
	}


	public static HashMap<String, String> getUserDetails(Context context,String name){
		SharedPreferences pref = context.getSharedPreferences(name, Context.MODE_PRIVATE);
		HashMap<String, String> user = new HashMap<String, String>();

		// user login


		user.put(USER_DOB, pref.getString(USER_ID, null));
		user.put(USER_SEX, pref.getString(USER_ID, null));
		user.put(USER_AGE, pref.getString(USER_ID, null));
		user.put(USER_CITY, pref.getString(USER_ID, null));
		user.put(USER_ADDRESS, pref.getString(USER_ID, null));
		user.put(USER_LAT, pref.getString(USER_ID, null));
		user.put(USER_LONG, pref.getString(USER_ID, null));
		user.put(USER_PROFILEIMAGE, pref.getString(USER_ID, null));


		user.put(USER_ID, pref.getString(USER_ID, null));
		user.put(USER_FIRSTNAME, pref.getString(USER_FIRSTNAME, null));
		user.put(USER_LASTNAME, pref.getString(USER_LASTNAME, null));
		user.put(USER_EMAILID, pref.getString(USER_EMAILID, null));
		user.put(ACCESSTOKEN, pref.getString(ACCESSTOKEN, null));
		user.put(USER_MOBILENO, pref.getString(USER_MOBILENO, null));



		return user;
	}




	//********************* Push notification Token *******************************************************************
	public static boolean savePushNotificationToken(Context context,String name, String token,String status) {


		SharedPreferences.Editor editor =context. getSharedPreferences(name, Context.MODE_PRIVATE).edit();
		editor.putString(TOKENKEY, token);
		editor.putString(SAVETOKEN, status);


		return editor.commit();
	}



	public static HashMap<String, String> getPushNotificationToken(Context context,String name){

		SharedPreferences pref = context.getSharedPreferences(name, Context.MODE_PRIVATE);
		HashMap<String, String> user = new HashMap<String, String>();

		// user login
		user.put(TOKENKEY, pref.getString(TOKENKEY, null));
		user.put(SAVETOKEN, pref.getString(SAVETOKEN, "No"));

		return user;
	}


//********************* clear data *******************************************************************

	public static void clear(Context context, String name ) {
		SharedPreferences.Editor editor = context.getSharedPreferences(name, Context.MODE_PRIVATE).edit();
		editor.clear();
		editor.commit();
	}
}
