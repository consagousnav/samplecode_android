package com.consagousframwork.model;

/**
 * Created by ADMIN1 on 3/16/2017.
 */

public class History_Been {


    String  userId, activityId, startTime, endTime, source, destination, url;


    public History_Been(String userId, String activityId, String startTime, String endTime, String source, String destination, String url) {
        this.userId = userId;
        this.activityId = activityId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.source = source;
        this.destination = destination;
        this.url = url;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
