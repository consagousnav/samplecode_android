package com.consagousframwork.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ADMIN1 on 3/21/2017.
 */

public class Contact_Been implements Parcelable {


    String name, number;
    int icCheck, position;


    public Contact_Been(String name, String number, int icCheck, int position) {
        this.name = name;
        this.number = number;
        this.icCheck = icCheck;
        this.position = position;
    }

    protected Contact_Been(Parcel in) {
        name = in.readString();
        number = in.readString();
        icCheck = in.readInt();
        position = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(number);
        dest.writeInt(icCheck);
        dest.writeInt(position);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Contact_Been> CREATOR = new Creator<Contact_Been>() {
        @Override
        public Contact_Been createFromParcel(Parcel in) {
            return new Contact_Been(in);
        }

        @Override
        public Contact_Been[] newArray(int size) {
            return new Contact_Been[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getIcCheck() {
        return icCheck;
    }

    public void setIcCheck(int icCheck) {
        this.icCheck = icCheck;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public static Creator<Contact_Been> getCREATOR() {
        return CREATOR;
    }
}
