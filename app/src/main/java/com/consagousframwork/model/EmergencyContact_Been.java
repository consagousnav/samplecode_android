package com.consagousframwork.model;

/**
 * Created by ADMIN1 on 3/18/2017.
 */

public class EmergencyContact_Been {

String contactId,contactType,contactName,contactMobileNo,contactSecoundNo;

    public EmergencyContact_Been(String contactId, String contactType, String contactName, String contactMobileNo, String contactSecoundNo) {
        this.contactId = contactId;
        this.contactType = contactType;
        this.contactName = contactName;
        this.contactMobileNo = contactMobileNo;
        this.contactSecoundNo = contactSecoundNo;
    }


    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactMobileNo() {
        return contactMobileNo;
    }

    public void setContactMobileNo(String contactMobileNo) {
        this.contactMobileNo = contactMobileNo;
    }

    public String getContactSecoundNo() {
        return contactSecoundNo;
    }

    public void setContactSecoundNo(String contactSecoundNo) {
        this.contactSecoundNo = contactSecoundNo;
    }
}
