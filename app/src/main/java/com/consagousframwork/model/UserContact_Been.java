package com.consagousframwork.model;

/**
 * Created by ADMIN1 on 3/24/2017.
 */

public class UserContact_Been {

     String id, contactType, name, mobileNo, secondaryMobileNo;


    public UserContact_Been(String id, String contactType, String name, String mobileNo, String secondaryMobileNo) {
        this.id = id;
        this.contactType = contactType;
        this.name = name;
        this.mobileNo = mobileNo;
        this.secondaryMobileNo = secondaryMobileNo;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getSecondaryMobileNo() {
        return secondaryMobileNo;
    }

    public void setSecondaryMobileNo(String secondaryMobileNo) {
        this.secondaryMobileNo = secondaryMobileNo;
    }
}
