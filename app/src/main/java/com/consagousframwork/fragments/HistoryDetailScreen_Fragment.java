package com.consagousframwork.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.consagousframwork.comman.Methods;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HistoryDetailScreen_Fragment extends Fragment implements View.OnKeyListener{

    private View rootView;

    @Bind(R.id.HistoryDetailScreen_WebView)
    WebView webView;
    @Bind(R.id.Header_Title)
    MyTextView tv_title;

    @Bind(R.id.Header_Left)
    MyTextView tv_left;
    @Bind(R.id.Header_Right)
    MyTextView tv_rigth;


    String str_url="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_history_detail_screen, container, false);

        ButterKnife.bind(this, rootView);

        if(getArguments().getString("URL")!=null){

            str_url = getArguments().getString("URL");

        }
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


        tv_title.setText(getActivity().getResources().getString(R.string.title_history));


        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webSettings.setBuiltInZoomControls(true);
//        webSettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webSettings.setSupportZoom(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);


//        webView.setFitsSystemWindows(true);

        Methods.printLog("URL=== "+str_url);


        webView.loadUrl(str_url);

    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        Methods.printLog("key press");
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            //do your stuff

            try{
                Methods.printLog("key press");

            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        return false;
    }
}
