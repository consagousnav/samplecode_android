package com.consagousframwork.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.consagousframwork.activityes.ContactScreen_Activity;
import com.consagousframwork.comman.Comman;
import com.consagousframwork.comman.Methods;
import com.consagousframwork.comman.Urls;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.model.Contact_Been;
import com.consagousframwork.network.UserInformation_AsyncTask;
import com.consagousframwork.others.DateAndTimeFormateConvertion;
import com.consagousframwork.others.DeviceHeightAndWith;
import com.consagousframwork.server_connection.ApiCalls;
import com.consagousframwork.session.SessionStore;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyEditTextView;
import com.consagousframwork.widgets.MyTextView;
import com.consagousframwork.widgets.PermissionScreen_Activity;
import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ADMIN1 on 3/22/2017.
 */

public class ProfileScreen_fragment extends Fragment implements ApiCallback {

    private View rootView;

    @Bind(R.id.RegistrationScreen_FName)
    MyEditTextView et_firstName;

    @Bind(R.id.RegistrationScreen_LName)
    MyEditTextView et_lasttName;

    @Bind(R.id.RegistrationScreen_Age)
    MyEditTextView et_age;

    @Bind(R.id.RegistrationScreen_City)
    MyEditTextView et_city;

    @Bind(R.id.RegistrationScreen_Country)
    MyEditTextView et_country;

    @Bind(R.id.RegistrationScreen_Address)
    MyEditTextView et_address;

    @Bind(R.id.RegistrationScreen_AboutMe)
    MyEditTextView et_aboutMe;

    @Bind(R.id.RegistrationScreen_PhoneNo)
    MyEditTextView et_phoneNo;

    @Bind(R.id.RegistrationScreen_EmailId)
    MyEditTextView et_emailId;

    @Bind(R.id.RegistrationScreen_Password)
    MyEditTextView et_password;

    @Bind(R.id.RegistrationScreen_DOB)
    MyTextView tv_dob;

    @Bind(R.id.RegistrationScreen_Sex)
    MyTextView tv_sex;

    @Bind(R.id.RegistrationScreen_ProfilePicture)
    SimpleDraweeView iv_profileImage;

    @Bind(R.id.RegistrationScreen_Contact)
    TagView tv_contact;


    @Bind(R.id.RegistrationScreen_ContactLabel)
    MyTextView tv_contactLable;

    @Bind(R.id.RegistrationScreen_EmergencyContact)
    TagView tv_emergencyContact;

    @Bind(R.id.RegistrationScreen_EmergencyContactLabale)
    MyTextView tv_emergencyContactLable;

    @Bind(R.id.Header_Title)
    MyTextView tv_title;


    @Bind(R.id.RegistrationScreen_EmergencyContactLayout)
    LinearLayout ll_emergencyContact;

    @Bind(R.id.RegistrationScreen_ContactLayout)
    LinearLayout ll_contact;

    @Bind(R.id.RegisterScreen_devider_Contact)
    View v_contact;

    @Bind(R.id.RegisterScreen_devider_EmergencyContact)
    View v_emergencyContact;

    @Bind(R.id.RegistrationScreen_PasswordLayout)
    LinearLayout ll_password;

    @Bind(R.id.RegisterScreen_devider_Password)
    View v_password;

    @Bind(R.id.RegistrationScreen_Header)
    LinearLayout ll_header;

    @Bind(R.id.RegisterScreen_Submit)
    Button but_saveChanges;


    private double dub_lat = 0.0, dub_long = 0.0;
    private String str_base64Image = "";
    private String gender_value = "";
    private Calendar c;
    ApiCalls apiCalls = new ApiCalls();

    List<Contact_Been> contactList = new ArrayList<Contact_Been>();
    List<Contact_Been> emergencyContactList = new ArrayList<Contact_Been>();

    int REQUEST_CONTACT = 500;
    int REQUEST_EMERGENCYCONTACT = 501;
    int REQUEST_GALLERY = 100;
    int REQUEST_CAMERA = 200;

    HashMap<String, String> hashMap = new HashMap<String, String>();
    private Context context;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate the layout for this fragment


        rootView = inflater.inflate(R.layout.activity_registration_screen_, container, false);

        ButterKnife.bind(this, rootView);

        context = getActivity();
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        dateFormatter = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        c = Calendar.getInstance();


        getCurrentLatLong();

        init();

    }



    //************************* getCurrentLatLong ***********************************************************************************************************************************
    private void init() {
        setDateTimeField(context);

        ll_emergencyContact.setVisibility(View.GONE);
        ll_contact.setVisibility(View.GONE);
        ll_password.setVisibility(View.GONE);
        v_contact.setVisibility(View.GONE);
        v_emergencyContact.setVisibility(View.GONE);
        v_password.setVisibility(View.GONE);

        tv_title.setText("Profile");


        but_saveChanges.setText(R.string.save_changes);
        setValue();

    }

    //************************* getValue ***********************************************************************************************************************************
    private void setValue() {

        hashMap = SessionStore.getUserDetails(getActivity(), Comman.USER_INFORMATION);


        try {


            Methods.showProgressDialog(getActivity(), "Please wait...");
            JSONObject json = new JSONObject();
            json.put("userId", hashMap.get(SessionStore.USER_ID));


            new UserInformation_AsyncTask(context, apiCalls, this).execute(json.toString(), Urls.BASE_URL + "userInfo", "123", "456", hashMap.get(SessionStore.ACCESSTOKEN));


        } catch (Exception e) {

            Methods.printLog(e.getMessage());
        }


    }


    //************************* getCurrentLatLong ***********************************************************************************************************************************
    private void getCurrentLatLong() {


       /* Intent i = new Intent(RegistrationScreen_Activity.this, PermissionScreen_Activity.class);
        i.putExtra("latlong")*/
    }


    //******************************** Click Event ***********************************************************************************************************************************

    @OnClick(R.id.RegistrationScreen_ProfilePicture)
    void profileImage() {


        showPictureModePopup();


    }


    @OnClick(R.id.RegistrationScreen_DOB)
    void dateOfBirth() {


        datePickerDialog.show();


    }

    @OnClick(R.id.RegistrationScreen_Sex)
    void sexButton() {

        showSexPopup();

    }


    @OnClick(R.id.RegistrationScreen_ContactLayout)
    void getContactList() {


        Intent intent = new Intent(context, ContactScreen_Activity.class);
        startActivityForResult(intent, REQUEST_CONTACT);


    }


    @OnClick(R.id.RegistrationScreen_EmergencyContactLayout)
    void getEmergencyContactList() {

        Intent intent = new Intent(context, ContactScreen_Activity.class);
        startActivityForResult(intent, REQUEST_EMERGENCYCONTACT);


    }


    @OnClick(R.id.RegisterScreen_Submit)
    void submit() {

        try {

            if (!et_emailId.getText().toString().trim().equalsIgnoreCase("")) {

                if (Methods.isValidEmail(et_emailId.getText().toString().trim())) {


                    if (!et_password.getText().toString().trim().equalsIgnoreCase("")) {


                        if (!et_firstName.getText().toString().trim().equalsIgnoreCase("")) {

                            if (!et_lasttName.getText().toString().trim().equalsIgnoreCase("")) {

                                if (!et_phoneNo.getText().toString().trim().equalsIgnoreCase("")) {

                                    if (!tv_dob.getText().toString().trim().equalsIgnoreCase("")) {
                                        if (!tv_sex.getText().toString().trim().equalsIgnoreCase("")) {

                                            if (!et_age.getText().toString().trim().equalsIgnoreCase("")) {

                                                if (!et_address.getText().toString().trim().equalsIgnoreCase("")) {
                                                    if (!et_city.getText().toString().trim().equalsIgnoreCase("")) {

                                                        if (!et_country.getText().toString().trim().equalsIgnoreCase("")) {
                                                            if (!str_base64Image.trim().equalsIgnoreCase("")) {


                                                                Methods.showProgressDialog(context, "Please wait...");

                                                                JSONObject json = new JSONObject();

                                                                json.put("name", et_firstName.getText().toString().trim());
                                                                json.put("surName", et_lasttName.getText().toString().trim());
                                                                json.put("email", et_emailId.getText().toString().trim());
                                                                json.put("password", et_password.getText().toString().trim());
                                                                json.put("mobileNo", et_phoneNo.getText().toString().trim());
                                                                json.put("dateOfBirth", DateAndTimeFormateConvertion.getEditStringToDate(tv_dob.getText().toString().trim()));
                                                                json.put("sex", tv_sex.getText().toString().trim());
                                                                json.put("age", et_age.getText().toString().trim());
                                                                json.put("country", et_country.getText().toString().trim());
                                                                json.put("city", et_city.getText().toString().trim());
                                                                json.put("address", et_address.getText().toString().trim());

                                                                json.put("latitude", "" + dub_lat);
                                                                json.put("longitude", "" + dub_long);

                                                                json.put("user_img", str_base64Image);


//                                                                new Registration_AsyncTask(ProfileScreen_fragment.this, apiCalls, ProfileScreen_fragment.this).execute(json.toString(), Urls.BASE_URL + "registration", "123", "456");


                                                            } else {

                                                                Methods.showToasMessage(context, getString(R.string.enter_profileimage));
                                                            }

                                                        } else {

                                                            Methods.showToasMessage(context, getString(R.string.enter_country));
                                                        }
                                                    } else {

                                                        Methods.showToasMessage(context, getString(R.string.enter_city));
                                                    }

                                                } else {

                                                    Methods.showToasMessage(context, getString(R.string.enter_address));
                                                }

                                            } else {

                                                Methods.showToasMessage(context, getString(R.string.enter_age));
                                            }
                                        } else {

                                            Methods.showToasMessage(context, getString(R.string.enter_sex));
                                        }

                                    } else {

                                        Methods.showToasMessage(context, getString(R.string.enter_dob));
                                    }

                                } else {

                                    Methods.showToasMessage(context, getString(R.string.enter_phone_no));
                                }

                            } else {

                                Methods.showToasMessage(context, getString(R.string.enter_last_name));
                            }

                        } else {

                            Methods.showToasMessage(context, getString(R.string.enter_first_name));
                        }
                    } else {

                        Methods.showToasMessage(context, getString(R.string.enter_password));
                    }

                } else {

                    Methods.showToasMessage(context, "Please enter valid email id");
                }


            } else {

                Methods.showToasMessage(context, getString(R.string.enter_email_id));
            }


        } catch (Exception e) {

            Methods.printLog(e.getMessage());

        }


    }






    //***************************** showPictureModePopup      ***********************************************************************************************************************

    private void showPictureModePopup() {

        MyTextView tv_Gallary, tv_Camera, tv_Cancel;

        LayoutInflater layoutInflater = (LayoutInflater)context. getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.select_gallary_option, null);

        tv_Gallary = (MyTextView) view.findViewById(R.id.SelectGallaryOption_Gallary);
        tv_Camera = (MyTextView) view.findViewById(R.id.SelectGallaryOption_Camera);
        tv_Cancel = (MyTextView) view.findViewById(R.id.SelectGallaryOption_Cancel);


        final Dialog dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;

        dialog.getWindow().setAttributes(lp);


        tv_Gallary.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent gallaryIntent = new Intent(context, PermissionScreen_Activity.class);
                gallaryIntent.putExtra("Value", "Gallery");
                startActivityForResult(gallaryIntent, REQUEST_GALLERY);

                dialog.dismiss();
            }
        });

        tv_Camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                Intent cameraIntent = new Intent(context, PermissionScreen_Activity.class);
                cameraIntent.putExtra("Value", "Camera");
                startActivityForResult(cameraIntent, REQUEST_CONTACT);
                dialog.dismiss();
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        dialog.show();


    }//End...


    //************* Activity for result *********************************************************************

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CONTACT && resultCode == getActivity().RESULT_OK) {

            getContactFromContact(data);

        } else if (requestCode == REQUEST_EMERGENCYCONTACT && resultCode == getActivity().RESULT_OK) {

            getEmergencyContactFromContact(data);

        } else if (resultCode == getActivity().RESULT_OK) {
            getImageSuccesss(requestCode, resultCode, data);

        }

    }

    private void getContactFromContact(Intent data) {


        if (data != null) {

            Bundle bundle = getActivity().getIntent().getExtras();
            contactList = data.getParcelableArrayListExtra("data");


            Methods.printLog("Size is===== " + contactList.size());

            ArrayList<Tag> tags = new ArrayList<>();
            Tag tag;


            for (int i = 0; i < contactList.size(); i++) {

                tag = new Tag(contactList.get(i).getName());
                tag.radius = 10f;
                tag.layoutColor = (getResources().getColor(R.color.appColor));


                tag.isDeletable = true;
                tags.add(tag);

            }
            tv_contact.addTags(tags);
            tv_contact.setVisibility(View.VISIBLE);

            tv_contactLable.setVisibility(View.GONE);


        }


    }

    private void getEmergencyContactFromContact(Intent data) {

        if (data != null) {

            Bundle bundle = getActivity().getIntent().getExtras();
            emergencyContactList = data.getParcelableArrayListExtra("data");


            Methods.printLog("Size is===== " + emergencyContactList.size());

            ArrayList<Tag> tags = new ArrayList<>();
            Tag tag;


            for (int i = 0; i < emergencyContactList.size(); i++) {

                tag = new Tag(emergencyContactList.get(i).getName());
                tag.radius = 10f;
                tag.layoutColor = (getResources().getColor(R.color.appColor));
                tag.isDeletable = true;
                tags.add(tag);

            }

            tv_emergencyContact.addTags(tags);
            tv_emergencyContact.setVisibility(View.VISIBLE);

            tv_emergencyContactLable.setVisibility(View.GONE);


        }

    }


    private void getImageSuccesss(int requestCode, int resultCode, Intent data) {


        // For Gating Image From Camara or Gallary
        if (requestCode == REQUEST_GALLERY && resultCode == getActivity().RESULT_OK && data != null) {


            String str_URI = data.getStringExtra("ImageURI");
            Uri selectedImage = Uri.parse(str_URI);
            beginCrop(selectedImage);

        } else if (requestCode == REQUEST_CAMERA && resultCode == getActivity().RESULT_OK) {


            Bitmap bbbmp = (Bitmap) data.getParcelableExtra("BitmapImage");
            Uri selectedImage = getImageUri(context, bbbmp);
            beginCrop(selectedImage);

        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(getActivity());


    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == getActivity().RESULT_OK) {


            try {
                Uri imageUri = Crop.getOutput(result);
                Bitmap bmp = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);


                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((DeviceHeightAndWith.getWidth(context) / 3), (DeviceHeightAndWith.getWidth(context) / 3));
                params.setMargins(0, 10, 0, 0);
                iv_profileImage.setLayoutParams(params);
//                iv_profileImage.setImageBitmap(bmp);
                iv_profileImage.setImageURI(imageUri);

                str_base64Image = Methods.getBase64String(bmp);


            } catch (Exception e) {

                Log.e("handleCrop", "handleCrop Exception: " + e.getMessage());
            }


        } else if (resultCode == Crop.RESULT_ERROR) {
            Methods.showToasMessage(context, Crop.getError(result).getMessage());

        }
    }


    //******************* ApiCallBack **********************************************************************************************************************************************
    @Override
    public void apiCallBackSuccess(JSONObject jsonResult, String from) {

        try {


            if (from.trim().equalsIgnoreCase("UserInformation_AsyncTask")) {


                if (jsonResult.getString("status").equalsIgnoreCase("1")) {


                    JSONArray jsonArray = jsonResult.getJSONArray("data");


                    et_firstName.setText(jsonArray.getJSONObject(0).getString("name"));
                    et_firstName.setSelection((jsonArray.getJSONObject(0).getString("name")).length());

                    et_lasttName.setText(jsonArray.getJSONObject(0).getString("surName"));
                    et_lasttName.setSelection((jsonArray.getJSONObject(0).getString("surName")).length());

                    et_emailId.setText(jsonArray.getJSONObject(0).getString("email"));
                    et_emailId.setSelection((jsonArray.getJSONObject(0).getString("email")).length());

                    et_phoneNo.setText(jsonArray.getJSONObject(0).getString("mobileNo"));
                    et_phoneNo.setSelection((jsonArray.getJSONObject(0).getString("mobileNo")).length());

                    et_age.setText(jsonArray.getJSONObject(0).getString("age"));
                    et_age.setSelection((jsonArray.getJSONObject(0).getString("age")).length());

                    et_city.setText(jsonArray.getJSONObject(0).getString("city"));
                    et_city.setSelection((jsonArray.getJSONObject(0).getString("city")).length());

                    et_country.setText(jsonArray.getJSONObject(0).getString("country"));
                    et_country.setSelection((jsonArray.getJSONObject(0).getString("country")).length());

                    et_address.setText(jsonArray.getJSONObject(0).getString("address"));
                    et_address.setSelection((jsonArray.getJSONObject(0).getString("address")).length());


                    tv_sex.setText(jsonArray.getJSONObject(0).getString("sex"));
                    tv_dob.setText(DateAndTimeFormateConvertion.getdateForAffiliatPa(jsonArray.getJSONObject(0).getString("dateOfBirth")));


                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((DeviceHeightAndWith.getWidth(context) / 3), (DeviceHeightAndWith.getWidth(context) / 3));
                    params.setMargins(0, 10, 0, 0);
                    iv_profileImage.setLayoutParams(params);
                    iv_profileImage.setImageURI(Uri.parse(jsonArray.getJSONObject(0).getString("user_img")));


/*

                    emergencyContactList.clear();
                    jsonArray = jsonResult.getJSONArray("contact");
*/


                   /* for (int i=0; i<jsonArray.length(); i++){

                        JSONObject jdata = jsonArray.getJSONObject(i);

                        emergencyContactList.add(new EmergencyContact_Been(jdata.getString("contactId"), jdata.getString("contactType"), jdata.getString("contactName"), jdata.getString("contactMobileNo"), jdata.getString("contactSecoundNo")));

                    }


                    Methods.printLog("emergencyContactList:   "+emergencyContactList.size());*/


                } else {

                    Methods.showToasMessage(context, jsonResult.getString("message"));
                }


            } else {

                Methods.showToasMessage(context, jsonResult.getString("message"));


            }


        } catch (Exception e) {


        }


    }


    //******************************** showPopup() *****************************************************************************************
    private void showSexPopup() {
        //		homePinFlage = false;
        final MyTextView tv_male, tv_female, tv_cancel, tv_title;
        LinearLayout ll_check;

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.select_sex_row, null);

        tv_male = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Male);
        tv_female = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Female);
        tv_cancel = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Cancel);
        tv_title = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Title);
        ll_check = (LinearLayout) view.findViewById(R.id.Select_Sex_Row_Check);

        final Dialog dialog = new Dialog(context, R.style.full_screen_dialog_withanimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);

        tv_title.setBold();
        tv_title.setText("Please select a sex");

        tv_male.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                tv_male.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                tv_female.setTextColor(context.getResources().getColor(R.color.colorBlack));
                gender_value = "Male";

                tv_sex.setText(gender_value);
                dialog.dismiss();


            }
        });


        tv_female.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                tv_male.setTextColor(context.getResources().getColor(R.color.colorBlack));
                tv_female.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                gender_value = "Female";
                tv_sex.setText(gender_value);
                dialog.dismiss();


            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = DeviceHeightAndWith.getWidth(context);
        lp.height = DeviceHeightAndWith.getHeight(context);
        lp.gravity = Gravity.TOP;

        dialog.getWindow().setAttributes(lp);

        dialog.show();

    }//End...

    private void setDateTimeField(final Context context) {

        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                tv_dob.setText((dateFormatter.format(newDate.getTime()).toString()));


            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        datePickerDialog.setTitle("select date");
        newCalendar.add(Calendar.MONTH, 0);
        newCalendar.set(Calendar.HOUR_OF_DAY, newCalendar.getMaximum(Calendar.HOUR_OF_DAY));
        newCalendar.set(Calendar.MINUTE, newCalendar.getMaximum(Calendar.MINUTE));
        newCalendar.set(Calendar.SECOND, newCalendar.getMaximum(Calendar.SECOND));
        newCalendar.set(Calendar.MILLISECOND, newCalendar.getMaximum(Calendar.MILLISECOND));
        datePickerDialog.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());


    }
}



