package com.consagousframwork.fragments;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bvapp.arcmenulibrary.ArcMenu;
import com.consagousframwork.comman.Comman;
import com.consagousframwork.comman.DownloadTask;
import com.consagousframwork.comman.Methods;
import com.consagousframwork.comman.Urls;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.interfaces.PoliLineCallBack;
import com.consagousframwork.network.GetRescue_AsyncTask;
import com.consagousframwork.network.Rescue_AsyncTask;
import com.consagousframwork.network.StartRescue_AsyncTask;
import com.consagousframwork.network.StopeRescue_AsyncTask;
import com.consagousframwork.network.Weather_AsyncTask;
import com.consagousframwork.others.DateAndTimeFormateConvertion;
import com.consagousframwork.others.MenuItemValue;
import com.consagousframwork.server_connection.ApiCalls;
import com.consagousframwork.session.SessionStore;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.consagousframwork.splesh.R.id.dayTextView;
import static com.consagousframwork.splesh.R.id.map;

public class MapScreen_Fragment extends FragmentActivity implements OnMapReadyCallback,
        com.google.android.gms.location.LocationListener,
        PoliLineCallBack, ApiCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {


    private GoogleMap mMap;
    boolean directionFlage = false;
    boolean rescueFlage = false;

    ArrayList<LatLng> markerPoints = new ArrayList<LatLng>();
    ArrayList<LatLng> tempmarkerPoints = new ArrayList<LatLng>();

    ApiCalls apiCalls = new ApiCalls();
    private HashMap<String, String> hashMap = new HashMap<String, String>();
    private String str_acrivity_id = "";
    private String str_batteryLevel = "";
    private String str_temperature = "";
    private String str_sunsetTime = "";


    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private boolean mRequestingLocationUpdates = true;
    private LocationRequest mLocationRequest;
    double latitude, longitude;
    LatLng defaultLocation = null;

    @Bind(R.id.weatherRelativeLayout)
    RelativeLayout rl_weather;


    @Bind(R.id.HomeScreen_fab)
    ArcMenu tv_startRescue;
    @Bind(R.id.HomeScreen_Stop)
    ImageView tv_stopRescue;


    @Bind(R.id.Header_Title)
    MyTextView tv_title;

    @Bind(R.id.Header_Left)
    MyTextView tv_left;
    @Bind(R.id.Header_Right)
    MyTextView tv_rigth;


    @Bind(R.id.currentTempTextView)
    MyTextView tv_currentTemp;
    @Bind(dayTextView)
    MyTextView tv_day;
    @Bind(R.id.maxminTextView)
    MyTextView tv_maxMinTemp;
    @Bind(R.id.citycountryTextView)
    MyTextView tv_cityCountryName;
    @Bind(R.id.mainweatherTextView)
    MyTextView tv_sky;
    @Bind(R.id.sunSetTimeTextView)
    MyTextView tv_sunsetTime;




    private PendingIntent pendingIntent;
    private AlarmManager manager;
    Timer timer;
    private int lastPosOfLatlong=0;

    boolean setMenuflage=true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_map_screen);

        ButterKnife.bind(this);

        tv_title.setText(getResources().getString(R.string.title_map));
        tv_left.setText("Done");
        tv_rigth.setText("Done");

        markerPoints.clear();

        hashMap = SessionStore.getUserDetails(MapScreen_Fragment.this, Comman.USER_INFORMATION);
        defaultLocation = new LatLng(Double.parseDouble(hashMap.get(SessionStore.USER_LAT)), Double.parseDouble(hashMap.get(SessionStore.USER_LONG)));


        createLocationRequest();

        buildGoogleApiClient();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);

        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));


        startTimer();
        init();

    }


    //************************** init ********************************************************************************************************************************************

    private void init(){


        tv_startRescue.showTooltip(true);


        tv_startRescue.setToolTipBackColor(Color.BLUE);
        tv_startRescue.setToolTipCorner(6f);
        tv_startRescue.setToolTipPadding(4f);
        tv_startRescue.setToolTipTextSize(14);
        tv_startRescue.setToolTipTextColor(Color.WHITE);

        tv_startRescue.setIcon(R.drawable.start_rescue);

    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        getCurrentLocations();
    }

    public void startTimer() {

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                // do your task here

//                Toast.makeText(MapScreen_Fragment.this, "Timer", Toast.LENGTH_LONG).show();

                try {


                    if (rescueFlage)  {



                        if(markerPoints.size()> lastPosOfLatlong){

                            lastPosOfLatlong = markerPoints.size();

                            JSONArray jsonArrayLatlong = new JSONArray();
                            JSONObject jsodata = new JSONObject();

                            for (int i=(lastPosOfLatlong-1); i<  markerPoints.size(); i++){

                                jsodata.put("latitude", markerPoints.get(i).latitude);
                                jsodata.put("longitude", markerPoints.get(i).longitude);

                            }

                            jsonArrayLatlong.put(jsodata);

                            JSONObject json = new JSONObject();
                            json.put("userId", hashMap.get(SessionStore.USER_ID));
                            json.put("activityId", str_acrivity_id);
                            json.put("rescueNote", "");
                            json.put("phoneBattery", str_batteryLevel);
                            json.put("temperature", str_temperature);
                            json.put("sunsetTime", str_sunsetTime);
                            json.put("latlong", jsonArrayLatlong);


                            new Rescue_AsyncTask(MapScreen_Fragment.this, apiCalls, MapScreen_Fragment.this).execute(json.toString(), Urls.BASE_URL + "activityAdd", "123", "456");

                        }











                    }

                } catch (Exception e) {


                }


            }
        }, 0, 30000);


    }





    //************************** Click Event ********************************************************************************************************************************************
    @OnClick(R.id.HomeScreen_fab)
    void ploatPathOnMap() {

        try {
            if (rescueFlage) {


                if(setMenuflage){


                    tv_startRescue.menuIn();


                    setMenuflage=false;
                    final int itemCount = MenuItemValue.ITEM_DRAWABLES.length;
                    for (int i = 0; i < itemCount; i++) {
                        ImageView item = new ImageView(MapScreen_Fragment.this);
                        item.setImageResource(MenuItemValue.ITEM_DRAWABLES[i]);

                        final int position = i;
                        tv_startRescue.addItem(item, MenuItemValue.STR[i], new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {



                                if(MenuItemValue.STR[position].trim().equalsIgnoreCase("Call")){
                                    try {
                                    JSONObject json = new JSONObject();
                                    json.put("activityId", str_acrivity_id);
                                    json.put("latitude", mLastLocation.getLatitude());
                                    json.put("longitude", mLastLocation.getLongitude());


                                    new GetRescue_AsyncTask(MapScreen_Fragment.this, apiCalls, MapScreen_Fragment.this).execute(json.toString(), Urls.BASE_URL + "rescueAdd", "123", "456");
                                    } catch (Exception e) {

                                        Methods.printLog("Start Rescue: " + e.getMessage());

                                    }

                                }else {

                                    Methods.showToasMessage(MapScreen_Fragment.this, "Pending...");
                                }


                            }
                        });
                    }

                }


                /*final int itemCount = MenuItemValue.ITEM_DRAWABLES.length;
                for (int i = 0; i < itemCount; i++) {
                    ImageView item = new ImageView(MapScreen_Fragment.this);
                    item.setImageResource(MenuItemValue.ITEM_DRAWABLES[i]);

                    final int position = i;
                    tv_startRescue.addItem(item, MenuItemValue.STR[i], new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {



                        }
                    });
                }
*/


                /*JSONObject json = new JSONObject();
                json.put("activityId", str_acrivity_id);
                json.put("latitude", mLastLocation.getLatitude());
                json.put("longitude", mLastLocation.getLongitude());


                new GetRescue_AsyncTask(MapScreen_Fragment.this, apiCalls, MapScreen_Fragment.this).execute(json.toString(), Urls.BASE_URL + "rescueAdd", "123", "456");*/

            } else {

                directionFlage = true;
                rescueFlage = true;

                mMap.clear();
                startLocationUpdates();

                JSONObject json = new JSONObject();
                json.put("userId", hashMap.get(SessionStore.USER_ID));
                json.put("latitude", mLastLocation.getLatitude());
                json.put("longitude", mLastLocation.getLongitude());
                new StartRescue_AsyncTask(MapScreen_Fragment.this, apiCalls, MapScreen_Fragment.this).execute(json.toString(), Urls.BASE_URL + "activityStart", "123", "456");

//                tv_startRescue.setIcon((R.color.t));
                tv_startRescue.setIcon(R.drawable.rescue);
                tv_stopRescue.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

            Methods.printLog("Start Rescue: " + e.getMessage());

        }
    }

    @OnClick(R.id.HomeScreen_Stop)
    void stopPloatingRoot() {

        directionFlage = false;
        rescueFlage = false;


        mMap.clear();
//        tv_startRescue.setIcon((R.drawable.start_rescue));
        tv_startRescue.setIcon(R.drawable.start_rescue);
        tv_stopRescue.setVisibility(View.GONE);
        startLocationUpdates();

        try {

            JSONObject json = new JSONObject();
            json.put("userId", hashMap.get(SessionStore.USER_ID));
            json.put("latitude", mLastLocation.getLatitude());
            json.put("longitude", mLastLocation.getLongitude());
            json.put("activityId", str_acrivity_id);

            new StopeRescue_AsyncTask(MapScreen_Fragment.this, apiCalls, MapScreen_Fragment.this).execute(json.toString(), Urls.BASE_URL + "activityStop", "123", "456");

        } catch (Exception e) {

            Methods.printLog("Stop Rescue: " + e.getMessage());

        }
        str_acrivity_id = "";
    }


    //*************************** get currenty location **************************************************************************************************************
    private void getCurrentLocations() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 1000);
            } else {

                mMap.setMyLocationEnabled(true);

            }
        } else {

            mMap.setMyLocationEnabled(true);

        }


        if (mMap.isMyLocationEnabled() && mMap.getMyLocation() != null) {
            defaultLocation = new LatLng(
                    mMap.getMyLocation().getLatitude(),
                    mMap.getMyLocation().getLongitude());

        }
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomGesturesEnabled(true);


    }


    //********************** onResume ******************************************************************************************************************************************************


    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    public void onResume() {
        super.onResume();
        hashMap = SessionStore.getUserDetails(MapScreen_Fragment.this, Comman.USER_INFORMATION);

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }


   /* public void onStop() {
        super.onStop();

        Log.e("MApppppppp", "MAPAMAPAMPA: onStop");

       *//* if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }*//*
    }
*/
    @Override
    protected void onDestroy() {
        super.onDestroy();



        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {

            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }

    }

    //************************** getBattery level *****************************************************************************************************************
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent intent) {

            if(Intent.ACTION_BATTERY_CHANGED == intent.getAction()){

                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                //  batteryTxt.setText(String.valueOf(level) + "%");
                str_batteryLevel = String.valueOf(level) + "%";

                Log.e("Battery level", "Battery level: " + String.valueOf(level) + "%");


            }

           }
    };

    private BroadcastReceiver mcallAddRescue = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent intent) {

            Log.e("Battery level", "mcallAddRescue ");

        }
    };


    //************************** Google APi connection Method ***************************************************************************************************************************


    protected void startLocationUpdates() {



        Log.e("sdfsdfsdf", "startLocationUpdates");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1002);
            } else {

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            }
        } else {

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }


    }


    protected void stopLocationUpdates() {

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }


    public void onConnectionFailed(ConnectionResult result) {

        Log.e("sdfsdfsdf", "stopLocationUpdates");

        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    public void onConnected(Bundle arg0) {

        Log.e("sdfsdfsdf", "onConnected" + mRequestingLocationUpdates);
        displayLocation();

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
            Log.e("mRequestingLoca", "onConnected");


        }
    }


    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }

    private void displayLocation() {


        Log.e("mRequestingLocatiotes", "onLocationChanged");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 1001);
            } else {

                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            }
        } else {

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        }


        if (mLastLocation != null) {

            new Weather_AsyncTask(MapScreen_Fragment.this, apiCalls, MapScreen_Fragment.this).execute("" + mLastLocation.getLatitude(), "" + mLastLocation.getLongitude());


            Methods.printLog(mLastLocation.getLatitude() + ", " + mLastLocation.getLongitude());

            if (directionFlage) {

                markerPoints.add(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));



                    if (markerPoints.size() >= 2) {
                        LatLng origin = markerPoints.get((markerPoints.size() - 2));
                        LatLng dest = markerPoints.get((markerPoints.size() - 1));
                        String url = Methods.getDirectionsUrl(origin, dest, markerPoints);

                        DownloadTask downloadTask = new DownloadTask(MapScreen_Fragment.this, MapScreen_Fragment.this);
                        downloadTask.execute(url);

                }
                LatLng currentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                mMap.addMarker(new MarkerOptions().position(currentLocation).title("" + markerPoints.size()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));



            } else {

                mMap.clear();
                LatLng currentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 20));
                mMap.addMarker(new MarkerOptions().position(currentLocation).title("My current location"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));


            }


        } else {
            Methods.showToasMessage(MapScreen_Fragment.this, "This application needs 'Location Services' to be turned on to use some of the features.");
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(MapScreen_Fragment.this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Comman.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(Comman.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(Comman.DISPLACEMENT);
    }


//******************** poliLineCallBackSuccess *******************************************************************

    @Override
    public void poliLineCallBackSuccess(PolylineOptions lineOptions) {

        if (lineOptions != null) {
            mMap.addPolyline(lineOptions);

        } else {

            Log.e("fgfdg", "Nullllll: ");
        }
    }


    //******************** apiCallBackSuccess *******************************************************************

    @Override
    public void apiCallBackSuccess(JSONObject jsonResult, String from) {


        Methods.printLog("" + jsonResult);
        try {


            if (from.trim().equalsIgnoreCase("StartRescue_AsyncTask")) {
                if (jsonResult.getString("status").equalsIgnoreCase("1")) {


                    str_acrivity_id = jsonResult.getString("activityId");


                    Methods.printLog("Jarray" + str_acrivity_id);

                    Methods.showToasMessage(MapScreen_Fragment.this, "Start rescue");

                } else {

                    Methods.showToasMessage(MapScreen_Fragment.this, jsonResult.getString("message"));

                }
            } else if (from.trim().equalsIgnoreCase("Weather_AsyncTask")) {


                if (jsonResult != null) {

                    rl_weather.setVisibility(View.VISIBLE);
                    str_sunsetTime = DateAndTimeFormateConvertion.getUnixTImeToRealTime(Long.parseLong(jsonResult.getJSONObject("sys").getString("sunset")));
                    str_temperature = Methods.getTempraturInKelvinToFahrenheit(Double.parseDouble(jsonResult.getJSONObject("main").getString("temp")));

                    tv_currentTemp.setText("" + (Double.parseDouble(jsonResult.getJSONObject("main").getString("temp"))) + (char) 0x00B0 + "F");

                    tv_day.setText("" + new SimpleDateFormat("EEEE").format(new Date()));

                    tv_cityCountryName.setText(jsonResult.getString("name") + ", " + jsonResult.getJSONObject("sys").getString("country"));

                    tv_maxMinTemp.setText("H" + (jsonResult.getJSONObject("main").getString("temp_max")) + (char) 0x00B0 + " L" + (jsonResult.getJSONObject("main").getString("temp_min")) + (char) 0x00B0);

                    tv_sky.setText(jsonResult.getJSONArray("weather").getJSONObject(0).getString("description"));
                    tv_sunsetTime.setText(str_sunsetTime);
                }


            } else if (from.trim().equalsIgnoreCase("GetRescue_AsyncTask")) {



                if (jsonResult.getString("status").equalsIgnoreCase("1")){

                    Methods.showToasMessage(MapScreen_Fragment.this, "Notification send to contacts,You will be rescued soon..");


                }else {

                    Methods.showToasMessage(MapScreen_Fragment.this, jsonResult.getString("message"));

                }



            }


        } catch (Exception e) {

            Methods.printLog("" + e.getMessage());
        }


    }


    //***************************** onRequestPermissionsResult *********************************************************************************************************************
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {


        switch (requestCode) {
            case 1000:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocations();

                } else {

                    Methods.showToasMessage(this, " GPRS permission denied");

                }

                break;

            case 1001:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayLocation();

                } else {

                    Methods.showToasMessage(this, " GPRS permission denied");

                }

                break;

            case 1002:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();

                } else {

                    Methods.showToasMessage(this, " GPRS permission denied");

                }

                break;
        }
    }


    //******************** isGooglePlayServicesAvailable *******************************************************************

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(MapScreen_Fragment.this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, MapScreen_Fragment.this, 0).show();
            return false;
        }
    }

}
