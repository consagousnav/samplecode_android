package com.consagousframwork.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.consagousframwork.adapter.UserContact_Adapter;
import com.consagousframwork.comman.Comman;
import com.consagousframwork.comman.Methods;
import com.consagousframwork.comman.Urls;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.interfaces.OnStartDragListener;
import com.consagousframwork.model.UserContact_Been;
import com.consagousframwork.network.GetUserContact_AsyncTask;
import com.consagousframwork.server_connection.ApiCalls;
import com.consagousframwork.session.SessionStore;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;
import com.consagousframwork.widgets.SimpleItemTouchHelperCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ContactScreen_Fragment extends Fragment implements ApiCallback, OnStartDragListener {

    private View rootView;

    @Bind(R.id.Header_Title)
    MyTextView tv_title;

    @Bind(R.id.Header_Left)
    MyTextView tv_left;
    @Bind(R.id.Header_Right)
    MyTextView tv_rigth;

    @Bind(R.id.ContactFragment_ListView)
    RecyclerView recyclerView;

    HashMap<String, String> hashMap = new HashMap<String, String>();


    ApiCalls apiCalls = new ApiCalls();
    List<UserContact_Been> contactList = new ArrayList<UserContact_Been>();
    private ItemTouchHelper mItemTouchHelper;
    UserContact_Adapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_contact_screen, container, false);

        ButterKnife.bind(this, rootView);


        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


        tv_title.setText(getActivity().getResources().getString(R.string.contact));
        init();


    }

    //******************** Init ***************************************************************************************
    private void init() {

        hashMap = SessionStore.getUserDetails(getActivity(), Comman.USER_INFORMATION);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());



        setValue();

    }

    //******************** setValue ***************************************************************************************

    private void setValue() {


        try {


            Methods.showProgressDialog(getActivity(), "Please wait...");
            JSONObject json = new JSONObject();
            json.put("userId", hashMap.get(SessionStore.USER_ID));

            new GetUserContact_AsyncTask(getActivity(), apiCalls, this).execute(json.toString(), Urls.BASE_URL+"userContactList", "123", "456");

        }catch (Exception e){

            Methods.printLog(""+e.getMessage());
        }


    }


    //************************************* apiCallBackSuccess ***********************************************************************************************

    @Override
    public void apiCallBackSuccess(JSONObject jsonResult, String from) {

        try {


            if (from.trim().equalsIgnoreCase("GetUserContact_AsyncTask")) {

                if (jsonResult.getString("status").equalsIgnoreCase("1")) {

                    contactList.clear();

                    JSONArray jArray = jsonResult.getJSONArray("data");

                    for (int i=0; i<jArray.length(); i++){

                        JSONObject data = jArray.getJSONObject(i);

                        contactList.add(new UserContact_Been(data.getString("id"), data.getString("contactType"), data.getString("name"),  data.getString("mobileNo"),  data.getString("secondaryMobileNo")));

                    }

                    sortInAse();


                    adapter = new UserContact_Adapter(getActivity(), contactList);

                    recyclerView.setAdapter(adapter);

                    ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
                    mItemTouchHelper = new ItemTouchHelper(callback);
                    mItemTouchHelper.attachToRecyclerView(recyclerView);

                } else {

                    Methods.showToasMessage(getActivity(), jsonResult.getString("message"));
                }


            }


        } catch (Exception e) {

            Methods.printLog(e.getMessage());

        }


    }



    //***************************** sortInAse *********************************************************************************************************************

    public void sortInAse() {
        Comparator<UserContact_Been> comparator = new Comparator<UserContact_Been>() {

            @Override
            public int compare(UserContact_Been object1, UserContact_Been object2) {


                return object1.getContactType().trim().compareToIgnoreCase(object2.getContactType().trim());
            }
        };
        Collections.sort(contactList, comparator);

    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
