package com.consagousframwork.fragments;

import android.app.LocalActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProfileScreen1_fragment extends Fragment implements FragmentTabHost.OnTabChangeListener {
    private View rootView;

    @Bind(R.id.Header_Title)
    MyTextView tv_title;
    @Bind(R.id.Header_Left)
    MyTextView tv_left;
    @Bind(R.id.Header_Right)
    MyTextView tv_rigth;


    private FragmentTabHost th_tabHost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile_screen, container, false);

        ButterKnife.bind(this, rootView);


        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        tv_title.setText(getActivity().getResources().getString(R.string.profile));

        th_tabHost = (FragmentTabHost) rootView.findViewById(R.id.fragmenttabhostTop);
        LocalActivityManager mLocalActivityManager = new LocalActivityManager(getActivity(), false);
        mLocalActivityManager.dispatchCreate(savedInstanceState); // state will be bundle your activity state which you get in onCreate
        th_tabHost.setup(getActivity(), getFragmentManager(), android.R.id.tabcontent);


        init();


    }

    private void init() {




        th_tabHost.addTab(setNewTab(getActivity(), th_tabHost, getResources().getString(R.string.tabhost_personalprofile), R.string.tabhost_personalprofile), ProfileScreen_fragment.class, null);
        th_tabHost.addTab(setNewTab(getActivity(), th_tabHost, getResources().getString(R.string.tabhost_personalprofile), R.string.tabhost_personalprofile), ProfileScreen_fragment.class, null);


        th_tabHost.getTabWidget().getChildAt(th_tabHost.getCurrentTab()).setBackgroundResource(R.color.appColor); // selected
        TextView tv_name = (TextView) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_Name); //for Selected Tab
        tv_name.setTextColor(getResources().getColor(R.color.colorWhite));
        View line = (View) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_line); //for Selected Tab
        line.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        th_tabHost.setOnTabChangedListener(this);



    }


    //******************** setNewTab ****************************************************************************************************************************************************
    private  FragmentTabHost.TabSpec setNewTab(Context context, FragmentTabHost tabHost, String tag, int title) {
       /* TabHost.TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getTabIndicator(tabHost.getContext(), title)); // new function to inject our own tab layout
        tabSpec.setContent(personalintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        tabSpec.setContent(personalintent);*/

        FragmentTabHost.TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getTabIndicator(tabHost.getContext(), title));

        return tabSpec;
    }

    private View getTabIndicator(Context context, int title) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        View line = (View) view.findViewById(R.id.TabHost_line);

        MyTextView tv = (MyTextView) view.findViewById(R.id.TabHost_Name);
        tv.setText(title);
        tv.setTextSize(10);
        tv.setBold();
        return view;
    }


    //******************** onTabChanged ****************************************************************************************************************************************************

    @Override
    public void onTabChanged(String tabId) {

        for (int i = 0; i < th_tabHost.getTabWidget().getChildCount(); i++) {

            th_tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.color.appColor); // unselected
            MyTextView tv = (MyTextView) th_tabHost.getTabWidget().getChildAt(i).findViewById(R.id.TabHost_Name); //Unselected Tabs
            tv.setTextColor(getResources().getColor(R.color.colorDarkGray));
            tv.setBold();
            View line = (View) th_tabHost.getTabWidget().getChildAt(i).findViewById(R.id.TabHost_line); //Unselected Tabs
            line.setBackgroundColor(getResources().getColor(R.color.colorDarkGray));

        }

        th_tabHost.getTabWidget().getChildAt(th_tabHost.getCurrentTab()).setBackgroundResource(R.color.appColor); // selected
        MyTextView tv = (MyTextView) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_Name); //for Selected Tab
        tv.setTextColor(getResources().getColor(R.color.colorWhite));
        tv.setBold();
        View line = (View) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_line); //for Selected Tab
        line.setBackgroundColor(getResources().getColor(R.color.colorWhite));


    }


}
