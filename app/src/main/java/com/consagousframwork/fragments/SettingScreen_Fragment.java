package com.consagousframwork.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.consagousframwork.comman.Comman;
import com.consagousframwork.activityes.Login_Registration_Screen_Activity;
import com.consagousframwork.session.SessionStore;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingScreen_Fragment extends Fragment {

    private View rootView;

    @Bind(R.id.Header_Title)
    MyTextView tv_title;
    @Bind(R.id.Header_Left)
    MyTextView tv_left;
    @Bind(R.id.Header_Right)
    MyTextView tv_rigth;




    private Fragment fr;
    private FragmentManager fm;
    private FragmentTransaction fragmentTransaction;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_setting_screen, container, false);

        ButterKnife.bind(this, rootView);


        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


        tv_title.setText(getActivity().getResources().getString(R.string.title_setting));
        tv_left.setText("Done");
        tv_rigth.setText("Done");


        init();


    }



    //****************** init ****************************************************************************************************************************************************************
    private void init(){






    }


    //****************** click event ****************************************************************************************************************************************************************

    @OnClick(R.id.SettingScreen_LogOut) void logOutMethod(){


        SessionStore.clear(getActivity(), Comman.USER_INFORMATION);
        startActivity(new Intent(getActivity(), Login_Registration_Screen_Activity.class));
        getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_side_out);
        getActivity().finish();


    }


    @OnClick(R.id.SettingScreen_profile) void profileMethod(){


        fr = new ProfileScreen_fragment();
        fm = getFragmentManager();
        fragmentTransaction = fm.beginTransaction();



        fragmentTransaction.replace(R.id.SettingScreen_fram, fr);
        fragmentTransaction.addToBackStack("name");
        fragmentTransaction.commit();


    }

    @OnClick(R.id.SettingScreen_Contact) void contactMethod(){


        fr = new ContactScreen_Fragment();
        fm = getFragmentManager();
        fragmentTransaction = fm.beginTransaction();



        fragmentTransaction.replace(R.id.SettingScreen_fram, fr);
        fragmentTransaction.addToBackStack("name");
        fragmentTransaction.commit();


    }



}
