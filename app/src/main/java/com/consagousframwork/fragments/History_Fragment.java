package com.consagousframwork.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.consagousframwork.adapter.History_Adapter;
import com.consagousframwork.network.History_AsyncTask;
import com.consagousframwork.model.History_Been;
import com.consagousframwork.comman.Comman;
import com.consagousframwork.comman.Methods;
import com.consagousframwork.comman.Urls;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.interfaces.ItemClickListener;
import com.consagousframwork.server_connection.ApiCalls;
import com.consagousframwork.session.SessionStore;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class History_Fragment extends Fragment implements ApiCallback, ItemClickListener {

    private View rootView;
    @Bind(R.id.Header_Title)
    MyTextView tv_title;

    @Bind(R.id.Header_Left)
    MyTextView tv_left;
    @Bind(R.id.Header_Right)
    MyTextView tv_rigth;

    @Bind(R.id.HistoryFragment_ListView)
    RecyclerView recyclerView;

    ApiCalls apiCalls = new ApiCalls();

    HashMap<String, String> hashMap = new HashMap<String, String>();
    List<History_Been> histroryList = new ArrayList<History_Been>();


    private Fragment fr;
    private FragmentManager fm;
    private FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_history, container, false);

        ButterKnife.bind(this, rootView);


        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


        tv_title.setText(getActivity().getResources().getString(R.string.title_history));
        tv_left.setText("Done");
        tv_rigth.setText("Done");
//        tv_rigth.setVisibility(View.VISIBLE);
        init();


    }


    //******************** Init ***************************************************************************************
    private void init() {

        hashMap = SessionStore.getUserDetails(getActivity(), Comman.USER_INFORMATION);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }


    //************************************* setValue ***********************************************************************************************
    private void setValue() {


        try {


            Methods.showProgressDialog(getActivity(), "Please wait...");
            histroryList.clear();

            JSONObject json = new JSONObject();
            json.put("userId", hashMap.get(SessionStore.USER_ID));

            new History_AsyncTask(getActivity(), apiCalls, this).execute(json.toString(), Urls.BASE_URL + "rescueList", "123", "456");


        } catch (Exception e) {

            Methods.printLog("" + e.getMessage());
        }


    }

    //****************************** onResume **********************************************************************************************************

    @Override
    public void onResume() {
        super.onResume();
        hashMap = SessionStore.getUserDetails(getActivity(), Comman.USER_INFORMATION);

        setValue();
    }


    //************************************* apiCallBackSuccess ***********************************************************************************************

    @Override
    public void apiCallBackSuccess(JSONObject jsonResult, String from) {

        try {


            if (from.trim().equalsIgnoreCase("History_AsyncTask")) {

                if (jsonResult.getString("status").equalsIgnoreCase("1")) {


                    JSONArray jarrary = jsonResult.getJSONArray("data");

                    for (int i = 0; i < jarrary.length(); i++) {


                        histroryList.add(new History_Been(jarrary.getJSONObject(i).getString("userId"), jarrary.getJSONObject(i).getString("activityId"), jarrary.getJSONObject(i).getString("startTime"), jarrary.getJSONObject(i).getString("endTime"), jarrary.getJSONObject(i).getString("source"), jarrary.getJSONObject(i).getString("destination"), jarrary.getJSONObject(i).getString("url")));


                    }


                    recyclerView.setAdapter(new History_Adapter(getActivity(), histroryList, this));

                } else {

                    Methods.showToasMessage(getActivity(), jsonResult.getString("message"));
                }


            }


        } catch (Exception e) {

            Methods.printLog(e.getMessage());

        }


    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {

        fr = new HistoryDetailScreen_Fragment();
        fm = getFragmentManager();
        fragmentTransaction = fm.beginTransaction();

        Bundle args = new Bundle();
        args.putString("URL", histroryList.get(position).getUrl());
        fr.setArguments(args);

        fragmentTransaction.replace(R.id.HistoryScreen_fram, fr);
        fragmentTransaction.addToBackStack("name");
        fragmentTransaction.commit();
    }
}
