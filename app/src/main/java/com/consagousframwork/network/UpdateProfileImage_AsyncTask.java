package com.consagousframwork.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import com.consagousframwork.comman.Methods;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.server_connection.ApiCalls;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by ADMIN1 on 3/9/2017.
 */

public class UpdateProfileImage_AsyncTask extends AsyncTask<String, Integer, JSONObject> {


    ApiCallback apiCallback;
    ApiCalls apiCalls;
    Context context;

    public UpdateProfileImage_AsyncTask(Context context, ApiCalls apiCalls , ApiCallback apiCallback ){

        this.apiCallback = apiCallback;
        this.apiCalls = apiCalls;
        this.context = context;


        // Methods.showProgressDialog(con);

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected JSONObject doInBackground(String... params) {


        JSONObject jsonResult=null;


        try {


            Uri imageUri = Uri.parse(params[4]);
            Bitmap bmp = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);

            File filesDir = context.getFilesDir();
            File imageFile = new File(filesDir, "imagefile" + ".jpg");

            OutputStream os;
            try {
                os = new FileOutputStream(imageFile);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }

            jsonResult = new JSONObject(apiCalls.postMethod( params[0], params[1], params[2],params[3], imageFile));


        }catch (Exception e){

            Methods.printLog(e.getMessage());
        }



        return jsonResult;
    }


    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

//        Methods.hideProgressDialog();
        apiCallback.apiCallBackSuccess(jsonObject, "UpdateProfileImage_AsyncTask");

    }
}
