package com.consagousframwork.network;

import android.content.Context;
import android.os.AsyncTask;

import com.consagousframwork.comman.Methods;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.server_connection.ApiCalls;

import org.json.JSONObject;

/**
 * Created by ADMIN1 on 3/9/2017.
 */

public class UserContactAdd_AsyncTask extends AsyncTask<String, Integer, JSONObject> {


    ApiCallback apiCallback;
    ApiCalls apiCalls;

    public UserContactAdd_AsyncTask(Context context, ApiCalls apiCalls , ApiCallback apiCallback ){

        this.apiCallback = apiCallback;
        this.apiCalls = apiCalls;

        // Methods.showProgressDialog(con);

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected JSONObject doInBackground(String... params) {


        JSONObject jsonResult=null;


        try {

            jsonResult = new JSONObject(apiCalls.postMethod( params[0], params[1], params[2], params[3]));


        }catch (Exception e){

            Methods.printLog(e.getMessage());
        }



        return jsonResult;
    }


    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

//        Methods.hideProgressDialog();
        apiCallback.apiCallBackSuccess(jsonObject, "GetRescue_AsyncTask");

    }
}
