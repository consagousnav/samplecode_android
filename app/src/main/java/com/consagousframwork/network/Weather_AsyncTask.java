package com.consagousframwork.network;

import android.content.Context;
import android.os.AsyncTask;

import com.consagousframwork.comman.Methods;
import com.consagousframwork.comman.Urls;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.server_connection.ApiCalls;
import com.consagousframwork.splesh.R;

import org.json.JSONObject;

/**
 * Created by ADMIN1 on 3/10/2017.
 */

public class Weather_AsyncTask  extends AsyncTask<String, Integer, JSONObject> {


    ApiCallback apiCallback;
    ApiCalls apiCalls;
    Context context;

    public  Weather_AsyncTask(Context context, ApiCalls apiCalls , ApiCallback apiCallback ){

        this.apiCallback = apiCallback;
        this.apiCalls = apiCalls;
        this.context = context;


        // Methods.showProgressDialog(con);

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected JSONObject doInBackground(String... params) {


        JSONObject jsonResult=null;


        try {


            //http://samples.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=ac86aaba9b3f9dc3f229a5a3788e58fc
            jsonResult = new JSONObject(apiCalls.getWeaterMethod(Urls.WEATER_URL+"lat="+params[0]+"&lon="+params[1]+"&cnt=7&mode=json&units=imperial&appid="+context.getResources().getString(R.string.openweathermap_api_key)));


        }catch (Exception e){

            Methods.printLog(e.getMessage());
        }



        return jsonResult;
    }


    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

//        Methods.hideProgressDialog();
        apiCallback.apiCallBackSuccess(jsonObject, "Weather_AsyncTask");

    }
}
