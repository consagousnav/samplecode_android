package com.consagousframwork.others;

import android.content.Context;
import android.util.DisplayMetrics;

public class DeviceHeightAndWith {

	
	  static DisplayMetrics displayMetrics ;
			
			
	public static int getWidth(Context context){
		
		displayMetrics = context.getResources().getDisplayMetrics();
		int width = displayMetrics.widthPixels;
		return width;
	}
	
	public static int getHeight(Context context){
		
		displayMetrics =context .getResources().getDisplayMetrics();
		int height = displayMetrics.heightPixels;
		
		return height;
		
	}
	
}
