package com.consagousframwork.others;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;



/**
 * Created by ACS on 21-10-15.
 */
public class DateAndTimeFormateConvertion {

	// Date Conversion 31/11/2014 To 31-Nov-2014
	public String DateConvertString(String str) {
		String formattedDate = "";
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(str);
			formattedDate = new SimpleDateFormat("dd-MMM-yyyy").format(date);

		} catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// Convert Date 2014/11/31 To 31-Nov-2014
	public String DateConvertStringEdit(String str) {
		String formattedDate = "";
		try {
			Date date = new SimpleDateFormat("yyyy/MM/dd").parse(str);
			formattedDate = new SimpleDateFormat("dd-MMM-yyyy").format(date);

		} catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// Convert Date 1/Nov/2014 To 1-11-2014
	public String DateConvertInteger(String str) {
		String DateString = "";
		try {
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			Date date = format1.parse(str);
			DateString = format2.format(date);
		} catch (Exception e) {
			Log.v("Exception ", "Date Convertion Exception :" + e.getMessage());

		}
		return DateString;
	}

	// Time Conversion

	// Time Convertion 13:30 To 1:30 PM
	public static String TimeConversion(String time) {
		String timeCon = "";
		if (time.equalsIgnoreCase("")) {
			return timeCon;
		} else {
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			Date date;
			try {
				date = dateFormat.parse(time);
				DateFormat dateFormate1 = new SimpleDateFormat("hh:mm a");
				timeCon = dateFormate1.format(date);// .toLowerCase(); //
				// "12:18am"
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return timeCon;
		}
	}

	//*********** get Current time in 24 hours *******************************************************************
	public static String getTimein_24(){

		Calendar cal1 = Calendar.getInstance();
		SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
		System.out.println( sdf1.format(cal1.getTime()) );

		return  sdf1.format(cal1.getTime()) ;
	}
	//*********** get Current time in 12 hours *******************************************************************
	public static String getTimein_12(){

		Calendar cal1 = Calendar.getInstance();
		SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
		System.out.println( sdf1.format(cal1.getTime()) );

		return  sdf1.format(cal1.getTime()).replaceAll(Pattern.quote("."), "") ;
	}

	// ******************* Get Current
	// Date**************************************************************************************************************
	public static String getCurrentDateAndTime() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		// get current date time with Date()
		Date date = new Date();

		return dateFormat.format(date).trim().replaceAll("/", "-");
	}

	
	// Date**************************************************************************************************************
		public static String getCurrentDate() {

			DateFormat dateFormat = new SimpleDateFormat("d/MMMM/yyyy");
			// get current date time with Date()
			Date date = new Date();

			return dateFormat.format(date).trim().replaceAll("/", "-");
		}

	// Date**************************************************************************************************************
	public static String getTwodaysDate() {

		String formattedDate="";
		try{
			Calendar cal = Calendar.getInstance(); // creates calendar
			cal.setTime(new Date()); // sets calendar time/date
			cal.add(Calendar.HOUR_OF_DAY, 48); // adds one hour
			cal.getTime(); // returns new date object, one hour in the future



			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd").parse(cal.get(Calendar.YEAR)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DATE));
			formattedDate = new SimpleDateFormat("MMMM dd, yyyy").format(selecteddate);
		}catch(Exception e){

			Log.e("Exceptions", "CurrentDate In String Exception: "+e.getMessage());
		}

		return formattedDate.trim().replaceAll("/", "-");
	}


	// Date**************************************************************************************************************
	public static String getNextdayDate(String str_date) {

		String ss[] = str_date.trim().split("-");

		String formattedDate="";
		try{
			Calendar cal = Calendar.getInstance(); // creates calendar
			cal.setTime(new Date()); // sets calendar time/date
			cal.set(Integer.parseInt(ss[0].trim()), (Integer.parseInt(ss[1].trim())-1), Integer.parseInt(ss[2].trim())); // sets calendar time/date

			cal.add(Calendar.HOUR_OF_DAY, 24); // adds one hour
			cal.getTime(); // returns new date object, one hour in the future



			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd").parse(cal.get(Calendar.YEAR)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DATE));
			formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(selecteddate);
		}catch(Exception e){

			Log.e("Exceptions", "CurrentDate In String Exception: "+e.getMessage());
		}

		return formattedDate.trim().replaceAll("/", "-");
	}


	// ******************* Get Current
	// Date**************************************************************************************************************
	public static String getCurrentDateInString(String date) {

		String formattedDate="";
		try{


			Date selecteddate = new SimpleDateFormat("dd/MM/yyyy").parse(date.trim().replaceAll("-", "/"));
			formattedDate = new SimpleDateFormat("MMM dd, yyyy").format(selecteddate);

		}catch(Exception e){

			Log.e("Exceptions", "CurrentDate In String Exception: "+e.getMessage());
		}

		return formattedDate;


	}
	
	// Date**************************************************************************************************************
		public static String getDateForSaveComapanyDate(String date) {

			String formattedDate="";
			try{


				Date selecteddate = new SimpleDateFormat("MMM dd, yyyy").parse(date.trim().replaceAll("-", "/"));
				formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(selecteddate);

			}catch(Exception e){

				Log.e("Exceptions", "CurrentDate In String Exception: "+e.getMessage());
			}

			return formattedDate.replaceAll("/", "-");


		}

	
	
	// Date**************************************************************************************************************
	public static String getDateInString(String date) {

		String formattedDate="";
		try{


			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd").parse(date.trim().replaceAll("-", "/"));
			formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(selecteddate);

		}catch(Exception e){

			Log.e("Exceptions", "CurrentDate In String Exception: "+e.getMessage());
		}

		return formattedDate.replaceAll("/", "-");


	}


	// GEt day from date**************************************************************************************************************
	public static String getDayFromDate(String input_date) {

		String formattedDate="";
		try{
			SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy");
			Date dt1=format1.parse(input_date);
			DateFormat format2=new SimpleDateFormat("EEEE"); 
			formattedDate=format2.format(dt1);

		}catch(Exception e){

			Log.e("Exceptions", "CurrentDate In String Exception: "+e.getMessage());
		}

		return formattedDate;


	}

	// ******************* Get Tomarrow
	// Date**************************************************************************************************************
	public static String getTomarrowDate() {

		String str_tomarrowDate = "";

		try {
			DateFormat currentdateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date currentdate = new Date();
			String curDate = currentdateFormat.format(currentdate);

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Date date = format.parse(curDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_YEAR, 1);

			str_tomarrowDate = format.format(calendar.getTime());
		} catch (Exception e) {

			Log.e("Exception",
					" Get Tomarrow Date Exception: " + e.getMessage());
		}
		return str_tomarrowDate.trim().replaceAll("/", "-");
	}

	// day Diffrence
	// ************************************************************************************************************

	public static long DateDiffrence(String date, String currentDate) {

		String dateStart = currentDate.replaceAll("-", "/") + " 09:29:58";
		String dateStop = date.replaceAll("-", "/") + " 09:29:58";
		long str_date = 0;


		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
			str_date = diffDays;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return str_date;
	}

	// ************* Date in 1:30 PM, May 28, 2015
	// **************************************************************************************
	public static String getScheduleAddAppointmentdate(String date) {
		// TODO Auto-generated method stub

		String str_date[] = date.split("T");

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd")
			.parse(str_date[0].trim().replaceAll("-", "/"));
			formattedDate = getTimeAmPm(str_date[1])+ ", "+ new SimpleDateFormat("MMMM dd, yyyy")
			.format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// **************************************************************************************
	public static String getScheduleAddAppointmentdateFromDate(String date) {
		// TODO Auto-generated method stub



		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("hh:mm a, MMMM dd, yyyy").parse(date.trim());
			formattedDate =  new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}
	
	
	
	// **************************************************************************************
		public static String getScheduleAdddateFromDate(String date) {
			// TODO Auto-generated method stub



			String formattedDate = "";
			try {

//				Date selecteddate = new SimpleDateFormat("hh:mm a, MMMM dd, yyyy").parse(date.trim());
//				formattedDate =  new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(selecteddate);

				
				Date selecteddate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").parse(date.trim().replaceAll("-", "/"));
				formattedDate =  new SimpleDateFormat("MMMM dd, yyyy HH:mm a").format(selecteddate);

				
			}

			catch (Exception e) {
				Log.v("Exception ", "Date Exception :" + e.getMessage());
			}
			return formattedDate.replaceAll("/", "-");
		}
		
		// **************************************************************************************
				public static String getScheduledateFromDate(String date) {
					// TODO Auto-generated method stub



					String formattedDate = "";
					try {

//						Date selecteddate = new SimpleDateFormat("hh:mm a, MMMM dd, yyyy").parse(date.trim());
//						formattedDate =  new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(selecteddate);

						
						Date selecteddate = new SimpleDateFormat("yyyy/M/d").parse(date.trim().replaceAll("-", "/"));
						formattedDate =  new SimpleDateFormat("yyyy/MM/dd").format(selecteddate);

						
					}

					catch (Exception e) {
						Log.v("Exception ", "Date Exception :" + e.getMessage());
					}
					return formattedDate.replaceAll("/", "-");
				}

	// **************************************************************************************
	public static String getScheduleFromDate(String date) {
		// TODO Auto-generated method stub



		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("hh:mm a, MMMM dd, yyyy").parse(date.trim());
			formattedDate =  new SimpleDateFormat("yyyy/MM/dd HH:mm a").format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}



	// ************* Date in 1:30 PM, May 28, 2015
	// **************************************************************************************
	public static String getScheduleEditAppointmentdate(String date) {
		// TODO Auto-generated method stub

		String formattedDate = "";
		try {
			if (date.contains("AM")) {

				String str_date[] = date.split("AM");
				Date selecteddate = new SimpleDateFormat("MMMM dd yyyy")
				.parse(str_date[1].trim().replaceAll(",", "").trim());
				formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(
						selecteddate).replaceAll("/", "-")
						+ " " + getTimeAmPmto(str_date[0].trim() + " am");

			} else if (date.contains("am")) {

				String str_date[] = date.split("am");
				Date selecteddate = new SimpleDateFormat("MMMM dd yyyy")
				.parse(str_date[1].replaceAll(",", "").trim());
				formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(
						selecteddate).replaceAll("/", "-")
						+ " " + getTimeAmPmto(str_date[0].trim() + " am");

			} else if (date.contains("PM")) {

				String str_date[] = date.split("PM");
				Date selecteddate = new SimpleDateFormat("MMMM dd yyyy")
				.parse(str_date[1].trim().replaceAll(",", "").trim());
				formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(
						selecteddate).replaceAll("/", "-")
						+ " " + getTimeAmPmto(str_date[0].trim() + " pm");

			} else {

				String str_date[] = date.split("pm");
				Date selecteddate = new SimpleDateFormat("MMMM dd yyyy")
				.parse(str_date[1].trim().replaceAll(",", "").trim());
				formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(
						selecteddate).replaceAll("/", "-")
						+ " " + getTimeAmPmto(str_date[0].trim() + " pm");

			}

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// ************* Date in 1:30 PM - May 28, 2015
	// **************************************************************************************
	public static String getScheduleAppointmentdate(String date) {
		// TODO Auto-generated method stub

		String str_date[] = date.split("T");

		String str_Time[]=str_date[1].split(":");

		for(int i=0; i<str_Time.length; i++)
		{
			Log.v("DateAndTimeClass", ""+(i+1)+" time ="+str_Time[i]);
		}

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd")
			.parse(str_date[0].trim().replaceAll("-", "/"));
			formattedDate = getTimeAmPm(str_date[1])
					+ " - "
					+ new SimpleDateFormat("MMMM dd, yyyy")
			.format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// ************* get Time in AM PM
	// **************************************************************************************

	public static String getTimeAmPm(String st_Date) {

		String formattedDate = "";
		try {
			Date date = new SimpleDateFormat("HH:mm:ss").parse(st_Date);
			formattedDate = new SimpleDateFormat("hh:mm a").format(date);
			formattedDate = formattedDate.replaceAll(Pattern.quote("."), "");
		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}


		return formattedDate;
	}

	// ************* get Time in AM PM to
	// 12:30:00**************************************************************************************

	public static String getTimeAmPmto(String st_Date) {

		String formattedDate = "";

		try {
			/*
			 * Date date = new
			 * SimpleDateFormat("hh:mm a").parse(st_Date.toLowerCase());
			 * formattedDate = new SimpleDateFormat("HH:MM:ss").format(date);
			 */

			SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			Date date = parseFormat.parse(st_Date.toUpperCase());

			formattedDate = displayFormat.format(date);

			Log.v("TimeInDateAndTimeClass", ""+parseFormat.format(date) + " = "+ displayFormat.format(date));
			System.out.println(parseFormat.format(date) + " = "
					+ displayFormat.format(date));

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}


		return formattedDate.replaceAll(Pattern.quote("."), "");
	}

	// ************* May 28, 2015
	// **************************************************************************************
	public static String getScheduleVaccinationdate(String date) {
		// TODO Auto-generated method stub

		String str_date[] = date.split("T");

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd")
			.parse(str_date[0].trim().replaceAll("-", "/"));
			formattedDate = new SimpleDateFormat("MMMM dd, yyyy")
			.format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// May 28, 2015
	// *************************************************************************
	public static String getdateMMDDYYY(String date) {
		// TODO Auto-generated method stub

		Log.v("Date And Time", "Date ; " + date);

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("MM/dd/yyyy").parse(date.trim().replaceAll("-", "/"));
			formattedDate = new SimpleDateFormat("MMMM dd, yyyy").format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// *************************************************************************
	public static String getdateMMMMDDYYY(String date) {
		// TODO Auto-generated method stub

		Log.v("Date And Time", "Date ; " + date);

		String formattedDate = "";
		try {

			//				Date selecteddate = new SimpleDateFormat("MM/dd/yyyy").parse(date.trim().replaceAll("-", "/"));
			//				formattedDate = new SimpleDateFormat("MMMM dd, yyyy").format(selecteddate);
			Date selecteddate = new SimpleDateFormat("dd/MMMM/yyyy").parse(date.trim().replaceAll("-", "/"));
			formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	// ************* May 28, 2015
	// **************************************************************************************
	public static String getTime(String date1) {
		// TODO Auto-generated method stub

		String str_date[] = date1.split("T");
		String formattedDate = "";
		try {
			Date date = new SimpleDateFormat("hh:mm:ss").parse(str_date[1]);
			formattedDate = new SimpleDateFormat("hh:mm a").format(date);
		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll(Pattern.quote("."), "");
	}

	// ************* May 28, 2015 to 2015-05-28
	// **************************************************************************************
	public static String getdate(String date) {
		// TODO Auto-generated method stub

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("MMMM dd, yyyy")
			.parse(date);
			formattedDate = new SimpleDateFormat("yyyy/MM/dd")
			.format(selecteddate);


		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}


	// **************************************************************************************
	public static String getdateForAffiliatPa(String date) {
		// TODO Auto-generated method stub
		Log.v("Date And Time", "Date ; " + date);

		
		
		String formattedDate = "";
		try {


			Date selecteddate = new SimpleDateFormat("dd/MM/yyyy").parse(date.trim().replaceAll("-", "/"));
			formattedDate = new SimpleDateFormat("MMMM dd, yyyy").format(selecteddate);


		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}


	// ********* Simpal date **************************
	// ************* May 28, 2015
	// **************************************************************************************
	public static String getsimpal(String date) {
		// TODO Auto-generated method stub

		// String str_date[]=date.split("T");

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("MMMM dd, yyyy")
			.parse(date.trim());
			formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(
					selecteddate).replaceAll("/", "-");

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}



	public static String getEditStringToDate(String date) {
		// TODO Auto-generated method stub

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("MMMM dd, yyyy")
			.parse(date);
			formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(
					selecteddate).replaceAll("-", "/");


		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate;
	}

	public static String getCurrenTime() {

		String str_time = "";
		try {

			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			str_time = dateFormat.format(cal.getTime());
			System.out.println(dateFormat.format(cal.getTime()));
		} catch (Exception e) {

		}

		return str_time;

	}

	// ************* May 28, 2015 to 01-02-2015
	// **************************************************************************************
	public static String getMenstrualLastdate(String date) {
		// TODO Auto-generated method stub

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("MMMM dd, yyyy")
			.parse(date);
			formattedDate = new SimpleDateFormat("yyyy/MM/dd")
			.format(selecteddate);


		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}

	public static String getFormatedDateForGetLog(String date) {
		// TODO Auto-generated method stub

		String str_date[] = date.split("T");

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd").parse(str_date[0].trim().replaceAll("-", "/"));
			formattedDate = new SimpleDateFormat("yyyy/MM/dd")
			.format(selecteddate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(selecteddate);
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			Date afterdate = calendar.getTime();
			formattedDate = new SimpleDateFormat("yyyy/MM/dd")
			.format(afterdate);


		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}


	public static String getfromdate(int value) {
		// TODO Auto-generated method stub


		String formattedDate = "";
		try {

			Calendar c= Calendar.getInstance();

			Calendar calender = Calendar.getInstance();
			String	st_Date= (calender.get(Calendar.DATE)-value)+"/"+ (calender.get(Calendar.MONTH)+1)+"/"+calender.get(Calendar.YEAR);

			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(st_Date);
			formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}

	// ************************ Cahrt value **************************************************************
	public static String getweekdate(String date) {
		// TODO Auto-generated method stub

		String str_date[] = date.split("T");

		String formattedDate = "";
		try {

			Date selecteddate = new SimpleDateFormat("yyyy/MM/dd").parse(str_date[0].trim().replaceAll("-", "/"));


			formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(selecteddate);

		}

		catch (Exception e) {
			Log.v("Exception ", "Date Exception :" + e.getMessage());
		}
		return formattedDate.replaceAll("/", "-");
	}


	public static String getDateOfWeek(int date)
	{
		String finalDay="";

		Calendar calender = Calendar.getInstance();

		calender.set(calender.get(Calendar.YEAR), (calender.get(Calendar.MONTH)), (calender.get(Calendar.DATE)-date));

		return ""+(calender.get(Calendar.DATE));
	}

	public static String getCurrentmonth(int date)
	{

		String finalDay="";
		Calendar calender = Calendar.getInstance();
		calender.set(calender.get(Calendar.YEAR), (calender.get(Calendar.MONTH)), (calender.get(Calendar.DATE)-date));

		return  getMonthForInt(calender.get(Calendar.MONTH));



	}
	public static String getMonthForInt(int num) {


		Date today = new Date();  

		today.setMonth(num);
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM");  
		String monthName = dateFormat.format(today);  

		return monthName;
	}


	public static String getPreviousmonth(int date)
	{

		String finalDay="";
		Calendar calender = Calendar.getInstance();


		calender.set(calender.get(Calendar.YEAR), (calender.get(Calendar.MONTH)+1-date), (calender.get(Calendar.DATE)-date));



		return ""+getMonthForInt(calender.get(Calendar.MONTH));


	}

	public static String getStartDate(int date)
	{

		Calendar calender = Calendar.getInstance();
		calender.set(calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), (calender.get(Calendar.DATE)-date));



		return getDate(1+"/"+(calender.get(Calendar.MONTH)+1)+"/"+calender.get(Calendar.YEAR));
	}

	public static String getEndDate(int date)
	{

		Calendar calender = Calendar.getInstance();
		calender.set(calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), (calender.get(Calendar.DATE)-date));

		return getDate(calender.getActualMaximum(Calendar.DAY_OF_MONTH)+"/"+(calender.get(Calendar.MONTH)+1)+"/"+calender.get(Calendar.YEAR));
	}


	public static String getDate(String st_Date) {
		// TODO Auto-generated method stub
		String formattedDate="";
		try
		{

			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(st_Date);
			formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);


		}

		catch(Exception e)
		{
			Log.v("Exception ", "Date Exception :"+e.getMessage());
		}
		return formattedDate;
	}

	public static int getDayOfMonth(int date)
	{

		Calendar calender = Calendar.getInstance();
		calender.set(calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), (calender.get(Calendar.DATE)-date));


		return calender.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static String DateDiffrenceForPost(String date, String currentDate) {

		String dateStart = currentDate.replaceAll("-", "/") ;
		String dateStop = date.replaceAll("-", "/") ;
		long str_date = 0;
		String str_day="";


		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long diff = d1.getTime() - d2.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = (int) ((d1.getTime() - d2.getTime()) / (1000 * 60 * 60 * 24));
			str_date = diffDays;

			System.out.println("diffSeconds: "+diffSeconds);
			System.out.println("diffMinutes: "+diffMinutes);
			System.out.println("diffHours: "+diffHours);
			System.out.println("diffDays: "+diffDays);



			if(diffDays>0){

				if(diffDays>0 && diffDays==1){

					str_day = diffDays+" day";

				}else if(diffDays>0 && diffDays<7){

					str_day = diffDays+" days";

				}else if(diffDays==7){

					str_day = "1 week";

				}else if(diffDays>7){


					if((diffDays%7)==0){

						str_day =(diffDays%7)+" weeks"; 
					}else{

						if((diffDays%7)==1){

							str_day =(diffDays/7)+" weeks "+(diffDays%7)+" day";

						}else{

							str_day =(diffDays/7)+" weeks "+(diffDays%7)+" days";

						}

					}



				}




			}else{


				if(diffHours==0){

					str_day = diffHours+" hour";	

				}else if(diffHours>0){

					str_day = diffHours+" hours";

				}

				if(diffMinutes>0){

					str_day = str_day+" "+diffMinutes+" mins";	
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return str_day;
	}


	public static long DateDiffrenceForCalender(String startDate, String endDate) {

		String dateStart = endDate.replaceAll("-", "/") ;
		String dateStop = startDate.replaceAll("-", "/") ;
		long str_date = 0;
		long str_day=0;


		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long diff = d1.getTime() - d2.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = (int) ((d1.getTime() - d2.getTime()) / (1000 * 60 * 60 * 24));
			str_date = diffDays;

			System.out.println("diffSeconds: "+diffSeconds);
			System.out.println("diffMinutes: "+diffMinutes);
			System.out.println("diffHours: "+diffHours);
			System.out.println("diffDays: "+diffDays);


			//			str_day = diffDays;
			if(diffDays>0){

				//str_day = diffDays;
				str_date = diffDays;
			}else{


				if(diffHours>=0){

					str_date = diffHours;

				}else if(diffMinutes>=0){

					str_date = diffMinutes;

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return str_date;
	}


	public static String getYear(int date)
	{

		String finalDay="";

		try{
			Calendar calender = Calendar.getInstance();
			calender.set(calender.get(Calendar.YEAR), (calender.get(Calendar.MONTH)), (calender.get(Calendar.DATE)-date));


			Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(calender.get(Calendar.DATE)+"/"+calender.get(Calendar.MONTH)+"/"+calender.get(Calendar.YEAR));
			finalDay = new SimpleDateFormat("yy").format(date1);

		}catch(Exception e){

			Log.e("Exception", "Get Year Exceptions: "+e.getMessage());
		}
		return finalDay;



	}


	// ************* Get Age From birth date
	// ***************************************************************************************************************


	public static String calculateAge(Date birthDate)
	{
		String str_age="";
		int years = 0;
		int months = 0;
		int days = 0;
		//create calendar object for birth day
		Calendar birthDay = Calendar.getInstance();
		birthDay.setTimeInMillis(birthDate.getTime());
		//create calendar object for current day
		long currentTime = System.currentTimeMillis();
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(currentTime);
		//Get difference between years
		years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
		int currMonth = now.get(Calendar.MONTH) + 1;
		int birthMonth = birthDay.get(Calendar.MONTH) + 1;
		//Get difference between months
		months = currMonth - birthMonth;
		//if month difference is in negative then reduce years by one and calculate the number of months.
		if (months < 0)
		{
			years--;
			months = 12 - birthMonth + currMonth;
			if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
				months--;
		} else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
		{
			years--;
			months = 11;
		}
		//Calculate the days
		if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
			days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
		else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
		{
			int today = now.get(Calendar.DAY_OF_MONTH);
			now.add(Calendar.MONTH, -1);
			days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
		} else
		{
			days = 0;
			if (months == 12)
			{
				years++;
				months = 0;
			}
		}
		//Create new Age object


		if(years==0){

			str_age = months+" month old";


		}else{
			str_age = years+" year old";

		}


		return str_age;
	}


	//************ Get age in int ***************************************************************************

	public static Integer getAgeInInt(Date birthDate) {
		int years = 0;
		int months = 0;
		int days = 0;
		//create calendar object for birth day
		Calendar birthDay = Calendar.getInstance();
		birthDay.setTimeInMillis(birthDate.getTime());
		//create calendar object for current day
		long currentTime = System.currentTimeMillis();
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(currentTime);
		//Get difference between years
		years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
		int currMonth = now.get(Calendar.MONTH) + 1;
		int birthMonth = birthDay.get(Calendar.MONTH) + 1;
		//Get difference between months
		months = currMonth - birthMonth;
		//if month difference is in negative then reduce years by one and calculate the number of months.
		if (months < 0)
		{
			years--;
			months = 12 - birthMonth + currMonth;
			if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
				months--;
		} else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
		{
			years--;
			months = 11;
		}
		//Calculate the days
		if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
			days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
		else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
		{
			int today = now.get(Calendar.DAY_OF_MONTH);
			now.add(Calendar.MONTH, -1);
			days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
		} else
		{
			days = 0;
			if (months == 12)
			{
				years++;
				months = 0;
			}
		}
		//Create new Age object




		return years;
	}


	public static String getUnixTImeToRealTime(Long unixTime){

		String str_time="";

		try {

			Date date = new Date(unixTime*1000L); // *1000 is to convert seconds to milliseconds
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a"); // the format of your date
			sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
			String formattedDate = sdf.format(date);
			str_time =formattedDate;


		}catch (Exception e){


		}



		return str_time;
	}
}
