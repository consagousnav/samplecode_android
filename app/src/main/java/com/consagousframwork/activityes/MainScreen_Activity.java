package com.consagousframwork.activityes;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.consagousframwork.fragments.MapScreen_Fragment;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;

public class MainScreen_Activity extends TabActivity implements TabHost.OnTabChangeListener{


    private TabHost th_tabHost;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen_);






        th_tabHost =  getTabHost();

        Intent map = new Intent(MainScreen_Activity.this,MapScreen_Fragment.class);
        Intent history = new Intent(MainScreen_Activity.this,HistoryScreen_Activity.class);
        Intent setting = new Intent(MainScreen_Activity.this,SettingScreen_Activity.class);

        setNewTab(MainScreen_Activity.this, th_tabHost, getResources().getString(R.string.tabhost_map), R.string.tabhost_map, map);
        setNewTab(MainScreen_Activity.this, th_tabHost, getResources().getString(R.string.tabhost_history), R.string.tabhost_history, history);
        setNewTab(MainScreen_Activity.this, th_tabHost, getResources().getString(R.string.tabhost_setting), R.string.tabhost_setting, setting);

        th_tabHost.getTabWidget().getChildAt(th_tabHost.getCurrentTab()).setBackgroundResource(R.color.appColor); // selected
        TextView tv_name = (TextView) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_Name); //for Selected Tab
        tv_name.setTextColor(getResources().getColor(R.color.colorWhite));
        View line = (View) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_line); //for Selected Tab
        line.setBackgroundColor(getResources().getColor(R.color.colorWhite));


        th_tabHost.setOnTabChangedListener(MainScreen_Activity.this);


    }


    //******************** setNewTab ****************************************************************************************************************************************************
    private void setNewTab(Context context, TabHost tabHost, String tag, int title, Intent personalintent) {
        TabHost.TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getTabIndicator(tabHost.getContext(), title)); // new function to inject our own tab layout
        // tabSpec.setContent(contentID);
        tabSpec.setContent(personalintent);
        tabHost.addTab(tabSpec);
    }

    private View getTabIndicator(Context context, int title) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        View line = (View) view.findViewById(R.id.TabHost_line);

        MyTextView tv = (MyTextView) view.findViewById(R.id.TabHost_Name);
        tv.setText(title);
        tv.setTextSize(10);
        tv.setBold();
        return view;
    }


    //******************** onTabChanged ****************************************************************************************************************************************************

    @Override
    public void onTabChanged(String tabId) {

        for (int i = 0; i < th_tabHost.getTabWidget().getChildCount(); i++) {

            th_tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.color.appColor); // unselected
            MyTextView tv = (MyTextView) th_tabHost.getTabWidget().getChildAt(i).findViewById(R.id.TabHost_Name); //Unselected Tabs
            tv.setTextColor(getResources().getColor(R.color.colorDarkGray));
            tv.setBold();
            View line = (View) th_tabHost.getTabWidget().getChildAt(i).findViewById(R.id.TabHost_line); //Unselected Tabs
            line.setBackgroundColor(getResources().getColor(R.color.colorDarkGray));

        }

        th_tabHost.getTabWidget().getChildAt(th_tabHost.getCurrentTab()).setBackgroundResource(R.color.appColor); // selected
        MyTextView tv = (MyTextView) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_Name); //for Selected Tab
        tv.setTextColor(getResources().getColor(R.color.colorWhite));
        tv.setBold();
        View line = (View) th_tabHost.getCurrentTabView().findViewById(R.id.TabHost_line); //for Selected Tab
        line.setBackgroundColor(getResources().getColor(R.color.colorWhite));


       /* if (th_tabHost.getCurrentTab()==0){


            tv_title.setText(getResources().getString(R.string.title_map));

        }else if (th_tabHost.getCurrentTab()==1) {

            tv_title.setText(getResources().getString(R.string.title_history));


        }*/

        }
}
