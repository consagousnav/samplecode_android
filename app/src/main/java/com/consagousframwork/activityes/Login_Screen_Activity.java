package com.consagousframwork.activityes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.consagousframwork.network.Login_AsyncTask;
import com.consagousframwork.comman.Comman;
import com.consagousframwork.comman.Methods;
import com.consagousframwork.comman.Urls;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.server_connection.ApiCalls;
import com.consagousframwork.session.SessionStore;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Login_Screen_Activity extends AppCompatActivity implements ApiCallback{

    //Maping widgets
    @Bind(R.id.LoginScreen_UserName)
    EditText et_userName;
    @Bind(R.id.LoginScreen_Password)
    EditText et_password;
    @Bind(R.id.Header_Title)
    MyTextView tv_title;

    ApiCalls apiCalls = new ApiCalls();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__screen_);

        ButterKnife.bind(this);



        tv_title.setText("Login");


    }


    //********************* onClick Event ******************************************************************************************************************************

    @OnClick(R.id.LoginScreen_Login)
    void LodinButton() {


        if(!et_userName.getText().toString().trim().equalsIgnoreCase("")){

            if(!et_password.getText().toString().trim().equalsIgnoreCase("")){

                try {

                    JSONObject json = new JSONObject();
                    json.put("email", et_userName.getText().toString().trim());
                    json.put("password", et_password.getText().toString().trim());


                    Methods.showProgressDialog(Login_Screen_Activity.this,"Please wait...");
                    new Login_AsyncTask(Login_Screen_Activity.this,apiCalls, Login_Screen_Activity.this).execute(json.toString(), Urls.BASE_URL+"login", "123", "456");


                }catch (Exception e){

                    Log.e("Exception", "Exception: "+e.getMessage());
                }



            }else{

                Methods.showToasMessage(Login_Screen_Activity.this, getString(R.string.enter_password));
            }


        }else{

            Methods.showToasMessage(Login_Screen_Activity.this, getString(R.string.user_name));
        }



    }


    @OnClick(R.id.LoginScreen_ForgotPassword)
    void forgotPassword() {

        EditText et_userNameEmailId;
        TextView tv_title;
        TextView tv_cancel;
        TextView tv_submit;


        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.forgotpassword_popup, null);

        tv_cancel = (TextView)view.findViewById(R.id.ForgotPassword_popup_Cancel);
        tv_title = (TextView)view.findViewById(R.id.ForgotPassword_popup_Cancel);
        tv_submit = (TextView)view.findViewById(R.id.ForgotPassword_popup_Cancel);
        et_userNameEmailId = (EditText) view.findViewById(R.id.ForgotPasswort_Popup_ForgotPassword);


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            dialog.dismiss();

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }


    //*********************** API CallBacks ************************************************************************************************************************************
    @Override
    public void apiCallBackSuccess(JSONObject jsonResult, String from) {

        Methods.printLog("Resver Responce: "+jsonResult);

        try {

            if(jsonResult!=null){

                if(jsonResult.getString("status").equalsIgnoreCase("1")){


                    JSONArray jsonArray = jsonResult.getJSONArray("data");

                    JSONObject jsonData = jsonArray.getJSONObject(0);

                    Methods.printLog("Resver Responce: "+jsonData);


                    SessionStore.save(Login_Screen_Activity.this, Comman.USER_INFORMATION, jsonData.getString("userId"), jsonData.getString("name"), jsonData.getString("surName"), jsonData.getString("email"), jsonData.getString("token") , jsonData.getString("mobileNo"), jsonData.getString("dateOfBirth")
                            , jsonData.getString("sex"), jsonData.getString("age"), jsonData.getString("country"), jsonData.getString("city"), jsonData.getString("address"), jsonData.getString("latitude")
                            , jsonData.getString("longitude"), jsonData.getString("user_img")
                    );

                    Intent intent =new Intent(Login_Screen_Activity.this, MainScreen_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_side_out);
                    finish();


                }else{

                    Methods.showToasMessage(Login_Screen_Activity.this, jsonResult.getString("message"));
                }

            }



        }catch (Exception e){

            Methods.printLog(e.getMessage());


        }


    }




}
