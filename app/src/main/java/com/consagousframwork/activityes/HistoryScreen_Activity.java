package com.consagousframwork.activityes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.consagousframwork.fragments.History_Fragment;
import com.consagousframwork.splesh.R;

public class HistoryScreen_Activity extends AppCompatActivity {

    private Fragment fr;
    private FragmentManager fm ;
    private FragmentTransaction fragmentTransaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_screen_);


        fr = new History_Fragment();
        fm = getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.HistoryScreen_fram, fr);
//        fragmentTransaction.addToBackStack("name");
        fragmentTransaction.commit();

    }


    @Override
    public void onBackPressed() {


        if (getFragmentManager().getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }


    }
}
