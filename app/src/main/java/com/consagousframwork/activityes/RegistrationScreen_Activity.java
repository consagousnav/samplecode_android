package com.consagousframwork.activityes;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import com.consagousframwork.comman.Methods;
import com.consagousframwork.comman.Urls;
import com.consagousframwork.interfaces.ApiCallback;
import com.consagousframwork.model.Contact_Been;
import com.consagousframwork.network.Registration_AsyncTask;
import com.consagousframwork.network.UpdateProfileImage_AsyncTask;
import com.consagousframwork.others.DateAndTimeFormateConvertion;
import com.consagousframwork.others.DeviceHeightAndWith;
import com.consagousframwork.server_connection.ApiCalls;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyEditTextView;
import com.consagousframwork.widgets.MyTextView;
import com.consagousframwork.widgets.PermissionScreen_Activity;
import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationScreen_Activity extends AppCompatActivity implements ApiCallback {

    @Bind(R.id.RegistrationScreen_FName)
    MyEditTextView et_firstName;
    @Bind(R.id.RegistrationScreen_LName)
    MyEditTextView et_lasttName;
    @Bind(R.id.RegistrationScreen_Age)
    MyEditTextView et_age;
    @Bind(R.id.RegistrationScreen_City)
    MyEditTextView et_city;
    @Bind(R.id.RegistrationScreen_Country)
    MyEditTextView et_country;
    @Bind(R.id.RegistrationScreen_Address)
    MyEditTextView et_address;
    @Bind(R.id.RegistrationScreen_AboutMe)
    MyEditTextView et_aboutMe;
    @Bind(R.id.RegistrationScreen_PhoneNo)
    MyEditTextView et_phoneNo;
    @Bind(R.id.RegistrationScreen_EmailId)
    MyEditTextView et_emailId;
    @Bind(R.id.RegistrationScreen_Password)
    MyEditTextView et_password;

    @Bind(R.id.RegistrationScreen_DOB)
    MyTextView tv_dob;
    @Bind(R.id.RegistrationScreen_Sex)
    MyTextView tv_sex;

    @Bind(R.id.RegistrationScreen_ProfilePicture)
    SimpleDraweeView iv_profileImage;

    @Bind(R.id.RegistrationScreen_Contact)
    TagView tv_contact;
    @Bind(R.id.RegistrationScreen_ContactLabel)
    MyTextView tv_contactLable;


    @Bind(R.id.RegistrationScreen_EmergencyContact)
    TagView tv_emergencyContact;
    @Bind(R.id.RegistrationScreen_EmergencyContactLabale)
    MyTextView tv_emergencyContactLable;


    @Bind(R.id.Header_Title)
    MyTextView tv_title;

    private double dub_lat = 0.0, dub_long = 0.0;
    private String str_base64Image = "";
    private String gender_value = "";
    private Calendar c;
    ApiCalls apiCalls = new ApiCalls();

    List<Contact_Been> contactList = new ArrayList<Contact_Been>();
    List<Contact_Been> emergencyContactList = new ArrayList<Contact_Been>();

    int REQUEST_CONTACT = 500;
    int REQUEST_EMERGENCYCONTACT = 501;
    int REQUEST_GALLERY = 100;
    int REQUEST_CAMERA = 200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_registration_screen_);
        ButterKnife.bind(this);

        c = Calendar.getInstance();

        tv_title.setText("Registration");
        getCurrentLatLong();

        init();

    }


    //************************* getCurrentLatLong ***********************************************************************************************************************************
    private void init() {


        tv_contact.setOnTagDeleteListener(new TagView.OnTagDeleteListener() {
            @Override
            public void onTagDeleted(TagView view, Tag tag, int position) {


                view.remove(position);
                contactList.remove(position);


                if (contactList.size() == 0) {

                    tv_contactLable.setVisibility(View.VISIBLE);
                    tv_contact.setVisibility(View.GONE);
                }


            }
        });


        tv_emergencyContact.setOnTagDeleteListener(new TagView.OnTagDeleteListener() {
            @Override
            public void onTagDeleted(TagView view, Tag tag, int position) {


                emergencyContactList.remove(position);
                view.remove(position);

                if (emergencyContactList.size() == 0) {

                    tv_emergencyContactLable.setVisibility(View.VISIBLE);
                    tv_emergencyContact.setVisibility(View.GONE);
                }


            }
        });

    }

    //************************* getCurrentLatLong ***********************************************************************************************************************************
    private void getCurrentLatLong() {


       /* Intent i = new Intent(RegistrationScreen_Activity.this, PermissionScreen_Activity.class);
        i.putExtra("latlong")*/
    }


    //******************************** Click Event ***********************************************************************************************************************************

    @OnClick(R.id.RegistrationScreen_ProfilePicture)
    void profileImage() {


        showPictureModePopup();


    }


    @OnClick(R.id.RegistrationScreen_DOB)
    void dateOfBirth() {


        showDialog(1);


    }

    @OnClick(R.id.RegistrationScreen_Sex)
    void sexButton() {

        showSexPopup();

    }


    @OnClick(R.id.RegistrationScreen_ContactLayout)
    void getContactList() {


        Intent intent = new Intent(RegistrationScreen_Activity.this, ContactScreen_Activity.class);
        startActivityForResult(intent, REQUEST_CONTACT);


    }


    @OnClick(R.id.RegistrationScreen_EmergencyContactLayout)
    void getEmergencyContactList() {

        Intent intent = new Intent(RegistrationScreen_Activity.this, ContactScreen_Activity.class);
        startActivityForResult(intent, REQUEST_EMERGENCYCONTACT);


    }


    @OnClick(R.id.RegisterScreen_Submit)
    void submit() {

        try {

            if (!et_emailId.getText().toString().trim().equalsIgnoreCase("")) {

                if (Methods.isValidEmail(et_emailId.getText().toString().trim())) {


                    if (!et_password.getText().toString().trim().equalsIgnoreCase("")) {


                        if (!et_firstName.getText().toString().trim().equalsIgnoreCase("")) {

                            if (!et_lasttName.getText().toString().trim().equalsIgnoreCase("")) {

                                if (!et_phoneNo.getText().toString().trim().equalsIgnoreCase("")) {

                                    if (!tv_dob.getText().toString().trim().equalsIgnoreCase("")) {
                                        if (!tv_sex.getText().toString().trim().equalsIgnoreCase("")) {

                                            if (!et_age.getText().toString().trim().equalsIgnoreCase("")) {

                                                if (!et_address.getText().toString().trim().equalsIgnoreCase("")) {
                                                    if (!et_city.getText().toString().trim().equalsIgnoreCase("")) {

                                                        if (!et_country.getText().toString().trim().equalsIgnoreCase("")) {
                                                            if (!str_base64Image.trim().equalsIgnoreCase("")) {


                                                                Methods.showProgressDialog(RegistrationScreen_Activity.this, "Please wait...");

                                                                JSONObject json = new JSONObject();

                                                                json.put("name", et_firstName.getText().toString().trim());
                                                                json.put("surName", et_lasttName.getText().toString().trim());
                                                                json.put("email", et_emailId.getText().toString().trim());
                                                                json.put("password", et_password.getText().toString().trim());
                                                                json.put("mobileNo", et_phoneNo.getText().toString().trim());
                                                                json.put("dateOfBirth", DateAndTimeFormateConvertion.getEditStringToDate(tv_dob.getText().toString().trim()));
                                                                json.put("sex", tv_sex.getText().toString().trim());
                                                                json.put("age", et_age.getText().toString().trim());
                                                                json.put("country", et_country.getText().toString().trim());
                                                                json.put("city", et_city.getText().toString().trim());
                                                                json.put("address", et_address.getText().toString().trim());

                                                                json.put("latitude", "" + dub_lat);
                                                                json.put("longitude", "" + dub_long);

                                                                json.put("user_img", str_base64Image);

                                                                Methods.printLog(""+json.toString());


                                                                new Registration_AsyncTask(RegistrationScreen_Activity.this, apiCalls, RegistrationScreen_Activity.this).execute(json.toString(), Urls.BASE_URL + "registration", "123", "456");


                                                            } else {

                                                                Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_profileimage));
                                                            }

                                                        } else {

                                                            Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_country));
                                                        }
                                                    } else {

                                                        Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_city));
                                                    }

                                                } else {

                                                    Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_address));
                                                }

                                            } else {

                                                Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_age));
                                            }
                                        } else {

                                            Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_sex));
                                        }

                                    } else {

                                        Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_dob));
                                    }

                                } else {

                                    Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_phone_no));
                                }

                            } else {

                                Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_last_name));
                            }

                        } else {

                            Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_first_name));
                        }
                    } else {

                        Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_password));
                    }

                } else {

                    Methods.showToasMessage(RegistrationScreen_Activity.this, "Please enter valid email id");
                }


            } else {

                Methods.showToasMessage(RegistrationScreen_Activity.this, getString(R.string.enter_email_id));
            }


        } catch (Exception e) {

            Methods.printLog(e.getMessage());

        }


    }


    //************************* Date and Time Picker *********************************************************************************
    //******* Date Picker Dialog *******************************************************************************
    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 0) {

            return new TimePickerDialog(this, mTimeSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);

        } else if (id == 1) {

            DatePickerDialog _date = new DatePickerDialog(this, datePickerListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE)) {
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                    try {

                       /* Calendar cc = Calendar.getInstance();
                        view.setMinDate(cc.getTimeInMillis());
                        cc.set(cc.get(Calendar.YEAR), cc.get(Calendar.MONTH), (cc.get(Calendar.DATE)));*/


                       /* if (year > cc.get(Calendar.YEAR))
                            view.updateDate(cc.get(Calendar.YEAR), cc.get(Calendar.MONTH), (cc.get(Calendar.DATE)));

                        if (monthOfYear > cc.get(Calendar.MONTH) && year == cc.get(Calendar.YEAR))
                            view.updateDate(cc.get(Calendar.YEAR), cc.get(Calendar.MONTH), (cc.get(Calendar.DATE)));

                        if (dayOfMonth > cc.get(Calendar.DATE) && year == cc.get(Calendar.YEAR) && monthOfYear == cc.get(Calendar.MONTH))
                            view.updateDate(cc.get(Calendar.YEAR), cc.get(Calendar.MONTH), (cc.get(Calendar.DATE)));*/

                    } catch (Exception e) {

                    }
                }
            };
            return _date;
        }


        return null;

    }

    /**
     * Callback received when the user "picks" a time in the dialog
     */
    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                    //   str_time = hourOfDay + ":" + minute + ":00";

                    // tv_dateandtime.setText(DateAndTimeFormateConvertion.getScheduleAddAppointmentdate(str_date + "T" + str_time));


                }
            };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

            // str_date = selectedYear+"-"+(selectedMonth+1)+"-"+selectedDay;
            //showDialog(0);


            tv_dob.setText(DateAndTimeFormateConvertion.getCurrentDateInString(String.valueOf(selectedDay) + "/" + String.valueOf(selectedMonth + 1) + "/" + String.valueOf(selectedYear)));

        }
    };


    //***************************** showPictureModePopup      ***********************************************************************************************************************

    private void showPictureModePopup() {

        MyTextView tv_Gallary, tv_Camera, tv_Cancel;

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.select_gallary_option, null);

        tv_Gallary = (MyTextView) view.findViewById(R.id.SelectGallaryOption_Gallary);
        tv_Camera = (MyTextView) view.findViewById(R.id.SelectGallaryOption_Camera);
        tv_Cancel = (MyTextView) view.findViewById(R.id.SelectGallaryOption_Cancel);


        final Dialog dialog = new Dialog(RegistrationScreen_Activity.this, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;

        dialog.getWindow().setAttributes(lp);


        tv_Gallary.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent gallaryIntent = new Intent(RegistrationScreen_Activity.this, PermissionScreen_Activity.class);
                gallaryIntent.putExtra("Value", "Gallery");
                startActivityForResult(gallaryIntent, REQUEST_GALLERY);

                dialog.dismiss();
            }
        });

        tv_Camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                Intent cameraIntent = new Intent(RegistrationScreen_Activity.this, PermissionScreen_Activity.class);
                cameraIntent.putExtra("Value", "Camera");
                startActivityForResult(cameraIntent, REQUEST_CONTACT);
                dialog.dismiss();
            }
        });

        tv_Cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        dialog.show();


    }//End...


    //************* Activity for result *********************************************************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CONTACT && resultCode == RESULT_OK) {

            getContactFromContact(data);

        } else if (requestCode == REQUEST_EMERGENCYCONTACT && resultCode == RESULT_OK) {

            getEmergencyContactFromContact(data);

        } else if (resultCode == RESULT_OK) {
            getImageSuccesss(requestCode, resultCode, data);

        }

    }

    private void getContactFromContact(Intent data) {


        if (data != null) {

            Bundle bundle = getIntent().getExtras();
            contactList = data.getParcelableArrayListExtra("data");


            Methods.printLog("Size is===== " + contactList.size());

            ArrayList<Tag> tags = new ArrayList<>();
            Tag tag;


            for (int i = 0; i < contactList.size(); i++) {

                tag = new Tag(contactList.get(i).getName());
                tag.radius = 10f;
                tag.layoutColor = ( getResources().getColor(R.color.appColor));


                tag.isDeletable = true;
                tags.add(tag);

            }
            tv_contact.addTags(tags);
            tv_contact.setVisibility(View.VISIBLE);

            tv_contactLable.setVisibility(View.GONE);


        }


    }

    private void getEmergencyContactFromContact(Intent data) {

        if (data != null) {

            Bundle bundle = getIntent().getExtras();
            emergencyContactList = data.getParcelableArrayListExtra("data");


            Methods.printLog("Size is===== " + emergencyContactList.size());

            ArrayList<Tag> tags = new ArrayList<>();
            Tag tag;


            for (int i = 0; i < emergencyContactList.size(); i++) {

                tag = new Tag(emergencyContactList.get(i).getName());
                tag.radius = 10f;
                tag.layoutColor = ( getResources().getColor(R.color.appColor));
                tag.isDeletable = true;
                tags.add(tag);

            }

            tv_emergencyContact.addTags(tags);
            tv_emergencyContact.setVisibility(View.VISIBLE);

            tv_emergencyContactLable.setVisibility(View.GONE);


        }

    }


    private void getImageSuccesss(int requestCode, int resultCode, Intent data) {


        // For Gating Image From Camara or Gallary
        if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK && data != null) {


            String str_URI = data.getStringExtra("ImageURI");
            Uri selectedImage = Uri.parse(str_URI);
            beginCrop(selectedImage);

        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {


            Bitmap bbbmp = (Bitmap) data.getParcelableExtra("BitmapImage");
            Uri selectedImage = getImageUri(RegistrationScreen_Activity.this, bbbmp);
            beginCrop(selectedImage);

        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(RegistrationScreen_Activity.this);


    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {


            try {
                Uri imageUri = Crop.getOutput(result);
                Bitmap bmp = MediaStore.Images.Media.getBitmap(RegistrationScreen_Activity.this.getContentResolver(), imageUri);


                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((DeviceHeightAndWith.getWidth(RegistrationScreen_Activity.this) / 3), (DeviceHeightAndWith.getWidth(RegistrationScreen_Activity.this) / 3));
                iv_profileImage.setLayoutParams(params);
                iv_profileImage.setImageURI(imageUri);


//                str_base64Image = Methods.getBase64String(bmp);
                str_base64Image = ""+imageUri;

                Log.e("sfsdfsdf", "path: "+imageUri);



            } catch (Exception e) {

                Log.e("handleCrop", "handleCrop Exception: " + e.getMessage());
            }


        } else if (resultCode == Crop.RESULT_ERROR) {
            Methods.showToasMessage(this, Crop.getError(result).getMessage());

        }
    }


    //******************* ApiCallBack **********************************************************************************************************************************************
    @Override
    public void apiCallBackSuccess(JSONObject jsonResult, String from) {

        try {


            if(from.trim().equalsIgnoreCase("Registration_AsyncTask")){


                if(jsonResult.getString("status").equalsIgnoreCase("1")) {


                    Methods.showToasMessage(RegistrationScreen_Activity.this, jsonResult.getString("message"));
                    new UpdateProfileImage_AsyncTask(RegistrationScreen_Activity.this, apiCalls, this).execute(jsonResult.getString("userId"), Urls.BASE_URL + "upload", "123", "456", str_base64Image);

                }else{

                    Methods.showToasMessage(RegistrationScreen_Activity.this, jsonResult.getString("message"));
                }
            }else if(from.trim().equalsIgnoreCase("UpdateProfileImage_AsyncTask")){

                Methods.showToasMessage(RegistrationScreen_Activity.this, jsonResult.getString("message"));
                Methods.printLog( ""+jsonResult.getString("message"));
            }




        } catch (Exception e) {


        }


    }


    //******************************** showPopup() *****************************************************************************************
    private void showSexPopup() {
        //		homePinFlage = false;
        final MyTextView tv_male, tv_female, tv_cancel, tv_title;
        LinearLayout ll_check;

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.select_sex_row, null);

        tv_male = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Male);
        tv_female = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Female);
        tv_cancel = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Cancel);
        tv_title = (MyTextView) view.findViewById(R.id.Select_Sex_Row_Title);
        ll_check = (LinearLayout) view.findViewById(R.id.Select_Sex_Row_Check);

        final Dialog dialog = new Dialog(RegistrationScreen_Activity.this, R.style.full_screen_dialog_withanimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);

        tv_title.setBold();
        tv_title.setText("Please select a sex");

        tv_male.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                tv_male.setTextColor(RegistrationScreen_Activity.this.getResources().getColor(R.color.colorPrimary));
                tv_female.setTextColor(RegistrationScreen_Activity.this.getResources().getColor(R.color.colorBlack));
                gender_value = "Male";

                tv_sex.setText(gender_value);
                dialog.dismiss();


            }
        });


        tv_female.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                tv_male.setTextColor(RegistrationScreen_Activity.this.getResources().getColor(R.color.colorBlack));
                tv_female.setTextColor(RegistrationScreen_Activity.this.getResources().getColor(R.color.colorPrimary));

                gender_value = "Female";
                tv_sex.setText(gender_value);
                dialog.dismiss();


            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = DeviceHeightAndWith.getWidth(RegistrationScreen_Activity.this);
        lp.height = DeviceHeightAndWith.getHeight(RegistrationScreen_Activity.this);
        lp.gravity = Gravity.TOP;

        dialog.getWindow().setAttributes(lp);

        dialog.show();

    }//End...
}
