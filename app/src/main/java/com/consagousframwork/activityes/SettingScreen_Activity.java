package com.consagousframwork.activityes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.consagousframwork.fragments.SettingScreen_Fragment;
import com.consagousframwork.splesh.R;

public class SettingScreen_Activity extends AppCompatActivity {


    private Fragment fr;
    private FragmentManager fm ;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_screen_);


        fr = new SettingScreen_Fragment();
        fm = getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.SettingScreen_fram, fr);
        fragmentTransaction.addToBackStack("name");
        fragmentTransaction.commit();

    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }



    }
}
