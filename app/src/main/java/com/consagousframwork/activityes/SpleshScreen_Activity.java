package com.consagousframwork.activityes;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.consagousframwork.comman.Comman;
import com.consagousframwork.session.SessionStore;
import com.consagousframwork.splesh.R;

import java.util.HashMap;

public class SpleshScreen_Activity extends AppCompatActivity {


    private Handler handler;
    HashMap<String, String> hashMap = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splesh_screen_);


        hashMap = SessionStore.getUserDetails(SpleshScreen_Activity.this, Comman.USER_INFORMATION);

        haldlerMethod();

       // Timestamp stamp = new Timestamp(1489151036);
//        k*9f/5f-459.67f
//        1.8 *(temp-273)+32;??



    }

    //****************** Handler Medhod *****************************************************************************************
    private void haldlerMethod(){

        handler = new Handler();

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Intent intent =null;
               /* intent = new Intent(SpleshScreen_Activity.this, Login_Registration_Screen_Activity.class);

                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_side_out);
                finish();*/

                if(hashMap.get(SessionStore.USER_ID)==null)
                {

                    intent = new Intent(SpleshScreen_Activity.this, Login_Registration_Screen_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_side_out);
                    finish();
                }
                else
                {
                    intent = new Intent(SpleshScreen_Activity.this, MainScreen_Activity.class);
                    intent.putExtra("ID", hashMap.get(SessionStore.USER_ID));
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_side_out);
                    finish();
                }


            }
        }, 2000);


    }
}
