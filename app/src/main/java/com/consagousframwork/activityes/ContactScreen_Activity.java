package com.consagousframwork.activityes;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.consagousframwork.adapter.Contact_Adapter;
import com.consagousframwork.comman.Methods;
import com.consagousframwork.model.Contact_Been;
import com.consagousframwork.splesh.R;
import com.consagousframwork.widgets.MyEditTextView;
import com.consagousframwork.widgets.MyTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactScreen_Activity extends AppCompatActivity {


    @Bind(R.id.ContactScreen_RecyclerView)
    RecyclerView recyclerView;
    Cursor cursor;
    String name, phonenumber;

    @Bind(R.id.Header_Title)
    MyTextView tv_title;

    @Bind(R.id.Header_Left)
    MyTextView tv_left;
    @Bind(R.id.Header_Right)
    MyTextView tv_rigth;

    @Bind(R.id.ContactScreen_Search)
    MyEditTextView et_search;

    List<Contact_Been> contactList = new ArrayList<Contact_Been>();
    Contact_Adapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list_);

        ButterKnife.bind(this);
        init();


    }


    private void init() {


        tv_title.setText(getResources().getString(R.string.contact));
        tv_left.setText(getResources().getString(R.string.crop__done));
        tv_rigth.setText(getResources().getString(R.string.crop__done));
        tv_rigth.setVisibility(View.VISIBLE);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());









        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 1000);
            } else {

                GetContactsIntoArrayList();

            }
        } else {

            GetContactsIntoArrayList();

        }





        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                adapter.filter(""+s);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    public void GetContactsIntoArrayList() {

        contactList.clear();


        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        int count=0;

        while (cursor.moveToNext()) {

            name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


            Methods.printLog("Name: " + name + ": " + phonenumber+" ::: "+count);

            contactList.add(new Contact_Been(name, phonenumber, 0, count));
            count++;
        }

        cursor.close();
//        sortInAse();


        adapter = new Contact_Adapter(ContactScreen_Activity.this, contactList);

        recyclerView.setAdapter(adapter);

    }

    //***************************** onRequestPermissionsResult *********************************************************************************************************************

    @OnClick(R.id.Header_Right)
    void done() {

        int count = 0;
        ArrayList<Contact_Been> contactListTemp = new ArrayList<Contact_Been>();

        contactListTemp.clear();

        for (int i = 0; i < contactList.size(); i++) {


            if (contactList.get(i).getIcCheck() == 1) {
                count++;

                contactListTemp.add(new Contact_Been(contactList.get(i).getName(), contactList.get(i).getNumber(), 1, contactList.get(i).getPosition()));
            }

        }


        if (count == 0) {

            Methods.showToasMessage(ContactScreen_Activity.this, "please select atleast one contact");

        } else {


            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("data", contactListTemp);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finish();


        }


    }


    //***************************** onRequestPermissionsResult *********************************************************************************************************************
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {


        switch (requestCode) {
            case 1000:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GetContactsIntoArrayList();

                } else {

                    Methods.showToasMessage(this, "Permission Canceled, Now your application cannot access CONTACTS.");

                }

                break;


        }
    }

    //***************************** sortInAse *********************************************************************************************************************

    public void sortInAse() {
        Comparator<Contact_Been> comparator = new Comparator<Contact_Been>() {

            @Override
            public int compare(Contact_Been object1, Contact_Been object2) {


                return object1.getName().trim().compareToIgnoreCase(object2.getName().trim());
            }
        };
        Collections.sort(contactList, comparator);

    }


}
